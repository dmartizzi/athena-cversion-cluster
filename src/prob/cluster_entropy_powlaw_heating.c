#include "copyright.h"
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"
#include "prob/math_functions.h"
#ifdef MHD
//#include <gsl/gsl_multifit.h>
#endif /* MHD */

/* ========================================================================== */
/* Prototypes and Definitions */

/* Global Variables for use in Potential, Initial and Boundary Conditions */
/* ========================================================================== */
static Real phi_nfw(const Real x);
static void set_vars();      /* use in problem() and read_restart() */
static Real grav_pot(const Real x1, const Real x2, const Real x3);
static Real drhodlogx(const Real rho, const Real x, const Real hh);
void RK4_dens(Real *x, Real *rho , const int n_entries);

/* Cooling and heating functions */
/* ========================================================================== */
static Real kT_keV(const Real P, const Real d);
static Real kT_keV_inverse(const Real T);
static Real L_23(const Real T);
static Real cool_only(const Real d, const Real P);
static Real coolheat_simple(const Real d, const Real P, const Real r, const Real dM, const Real dt);
static Real coolheat_density(const Real d, const Real P, const Real r, const Real avgd, const Real dM, const Real dt);
static Real coolheat_mechanical(const Real d, const Real P, const Real r, const Real dM, const Real dt);
static void integrate_coolheat(DomainS *pDomain, const Real dt_hydro, const Real dM, const Real t_hydro, const int LevelMax);
static Real enforce_floor(DomainS *pDomain);
static Real star_formation(DomainS *pDomain);
static Real FindProblemCell(DomainS *pDomain);
static Real InwardMassFlux(DomainS *pDomain, const Real racc);
static Real Central_Cooling(DomainS *pDomain);
static void cool_step(DomainS *pDomain, const Real dt, const Real dM, const Real t_hydro, const int iter, const int n_sub, const int LevelMax);
double ran2(long int *idum);

/* set up the cooling curve. so that it can be used in L_23 */
/* --------------------------------------------------------------------------------------------- */
static Real logkT_SD[250]        = {-3.06462715, -3.04655487, -3.02848258, -3.01041029, -2.992338  ,
                                    -2.97426571, -2.95619342, -2.93812113, -2.92004884, -2.90197655,
                                    -2.88390426, -2.86583197, -2.84775968, -2.8296874 , -2.81161511,
                                    -2.79354282, -2.77547053, -2.75739824, -2.73932595, -2.72125366,
                                    -2.70318137, -2.68510908, -2.66703679, -2.6489645 , -2.63089221,
                                    -2.61281993, -2.59474764, -2.57667535, -2.55860306, -2.54053077,
                                    -2.52245848, -2.50438619, -2.4863139 , -2.46824161, -2.45016932,
                                    -2.43209703, -2.41402474, -2.39595246, -2.37788017, -2.35980788,
                                    -2.34173559, -2.3236633 , -2.30559101, -2.28751872, -2.26944643,
                                    -2.25137414, -2.23330185, -2.21522956, -2.19715727, -2.17908499,
                                    -2.1610127 , -2.14294041, -2.12486812, -2.10679583, -2.08872354,
                                    -2.07065125, -2.05257896, -2.03450667, -2.01643438, -1.99836209,
                                    -1.98028981, -1.96221752, -1.94414523, -1.92607294, -1.90800065,
                                    -1.88992836, -1.87185607, -1.85378378, -1.83571149, -1.8176392 ,
                                    -1.79956691, -1.78149462, -1.76342234, -1.74535005, -1.72727776,
                                    -1.70920547, -1.69113318, -1.67306089, -1.6549886 , -1.63691631,
                                    -1.61884402, -1.60077173, -1.58269944, -1.56462715, -1.54655487,
                                    -1.52848258, -1.51041029, -1.492338  , -1.47426571, -1.45619342,
                                    -1.43812113, -1.42004884, -1.40197655, -1.38390426, -1.36583197,
                                    -1.34775968, -1.3296874 , -1.31161511, -1.29354282, -1.27547053,
                                    -1.25739824, -1.23932595, -1.22125366, -1.20318137, -1.18510908,
                                    -1.16703679, -1.1489645 , -1.13089221, -1.11281993, -1.09474764,
                                    -1.07667535, -1.05860306, -1.04053077, -1.02245848, -1.00438619,
                                    -0.9863139 , -0.96824161, -0.95016932, -0.93209703, -0.91402474,
                                    -0.89595246, -0.87788017, -0.85980788, -0.84173559, -0.8236633 ,
                                    -0.80559101, -0.78751872, -0.76944643, -0.75137414, -0.73330185,
                                    -0.71522956, -0.69715727, -0.67908499, -0.6610127 , -0.64294041,
                                    -0.62486812, -0.60679583, -0.58872354, -0.57065125, -0.55257896,
                                    -0.53450667, -0.51643438, -0.49836209, -0.48028981, -0.46221752,
                                    -0.44414523, -0.42607294, -0.40800065, -0.38992836, -0.37185607,
                                    -0.35378378, -0.33571149, -0.3176392 , -0.29956691, -0.28149462,
                                    -0.26342234, -0.24535005, -0.22727776, -0.20920547, -0.19113318,
                                    -0.17306089, -0.1549886 , -0.13691631, -0.11884402, -0.10077173,
                                    -0.08269944, -0.06462715, -0.04655487, -0.02848258, -0.01041029,
				     0.007662  ,  0.02573429,  0.04380658,  0.06187887,  0.07995116,
				     0.09802345,  0.11609574,  0.13416803,  0.15224032,  0.1703126 ,
				     0.18838489,  0.20645718,  0.22452947,  0.24260176,  0.26067405,
				     0.27874634,  0.29681863,  0.31489092,  0.33296321,  0.3510355 ,
				     0.36910779,  0.38718007,  0.40525236,  0.42332465,  0.44139694,
				     0.45946923,  0.47754152,  0.49561381,  0.5136861 ,  0.53175839,
				     0.54983068,  0.56790297,  0.58597526,  0.60404754,  0.62211983,
				     0.64019212,  0.65826441,  0.6763367 ,  0.69440899,  0.71248128,
				     0.73055357,  0.74862586,  0.76669815,  0.78477044,  0.80284273,
				     0.82091501,  0.8389873 ,  0.85705959,  0.87513188,  0.89320417,
				     0.91127646,  0.92934875,  0.94742104,  0.96549333,  0.98356562,
				     1.00163791,  1.01971019,  1.03778248,  1.05585477,  1.07392706,
				     1.09199935,  1.11007164,  1.12814393,  1.14621622,  1.16428851,
				     1.1823608 ,  1.20043309,  1.21850538,  1.23657766,  1.25464995,
				     1.27272224,  1.29079453,  1.30886682,  1.32693911,  1.3450114 ,
				     1.36308369,  1.38115598,  1.39922827,  1.41730056,  1.43537285};

static Real logLambda_23_SD[250] = {-2.83      , -2.34566265, -1.8613253 , -1.39975904, -1.01301205,
                                    -0.62626506, -0.27493976,  0.03590361,  0.34674699,  0.5639759 ,
				    0.74108434,  0.91819277,  0.98060241,  1.03481928,  1.07879518,
				    1.07156627,  1.06433735,  1.0513253 ,  1.02963855,  1.00795181,
				    0.99542169,  0.98819277,  0.98096386,  0.98626506,  0.99349398,
				    1.00216867,  1.02385542,  1.04554217,  1.06843373,  1.09373494,
				    1.11903614,  1.14843373,  1.18096386,  1.21349398,  1.2460241 ,
				    1.27855422,  1.31120482,  1.3473494 ,  1.38349398,  1.4186747 ,
				    1.45120482,  1.48373494,  1.51626506,  1.54879518,  1.5813253 ,
				    1.61120482,  1.64012048,  1.66903614,  1.69096386,  1.7126506 ,
				    1.73289157,  1.7473494 ,  1.76180723,  1.77156627,  1.77518072,
				    1.77879518,  1.77759036,  1.7739759 ,  1.77036145,  1.77325301,
				    1.77686747,  1.78144578,  1.79228916,  1.80313253,  1.8139759 ,
				    1.82481928,  1.83566265,  1.84433735,  1.85156627,  1.85879518,
				    1.8660241 ,  1.87325301,  1.88048193,  1.88771084,  1.89493976,
				    1.89566265,  1.88120482,  1.86674699,  1.83493976,  1.78795181,
				    1.74096386,  1.66626506,  1.58313253,  1.5       ,  1.42771084,
				    1.35542169,  1.29072289,  1.25096386,  1.21120482,  1.18325301,
				    1.16879518,  1.15433735,  1.14746988,  1.14385542,  1.14024096,
				    1.13662651,  1.13301205,  1.12638554,  1.1046988 ,  1.08301205,
				    1.0526506 ,  1.00927711,  0.96590361,  0.92710843,  0.89096386,
				    0.85481928,  0.83746988,  0.82301205,  0.80891566,  0.79807229,
				    0.78722892,  0.77759036,  0.77036145,  0.76313253,  0.76      ,
				    0.76      ,  0.76      ,  0.76      ,  0.76      ,  0.76012048,
				    0.76373494,  0.7673494 ,  0.76710843,  0.75626506,  0.74542169,
				    0.72554217,  0.69662651,  0.66771084,  0.63349398,  0.5973494 ,
				    0.56120482,  0.52855422,  0.4960241 ,  0.46566265,  0.4439759 ,
				    0.42228916,  0.40216867,  0.38409639,  0.3660241 ,  0.35518072,
				    0.34795181,  0.34072289,  0.33024096,  0.31939759,  0.30903614,
				    0.30180723,  0.29457831,  0.2873494 ,  0.28012048,  0.27289157,
				    0.26566265,  0.25843373,  0.25120482,  0.24698795,  0.24337349,
				    0.24024096,  0.24385542,  0.24746988,  0.25108434,  0.2546988 ,
				    0.25831325,  0.26      ,  0.26      ,  0.26      ,  0.26277108,
				    0.26638554,  0.27      ,  0.27      ,  0.27      ,  0.26831325,
				    0.26108434,  0.25385542,  0.24831325,  0.2446988 ,  0.24108434,
				    0.23746988,  0.23385542,  0.23024096,  0.23      ,  0.23      ,
				    0.23      ,  0.23      ,  0.23      ,  0.23144578,  0.23506024,
				    0.2386747 ,  0.24228916,  0.24590361,  0.24951807,  0.25313253,
				    0.25674699,  0.26036145,  0.2639759 ,  0.26759036,  0.27120482,
				    0.27481928,  0.27843373,  0.28409639,  0.2913253 ,  0.29855422,
				    0.30578313,  0.31301205,  0.32024096,  0.32746988,  0.3346988 ,
				    0.34096386,  0.34457831,  0.34819277,  0.35361446,  0.36084337,
				    0.36807229,  0.3753012 ,  0.38253012,  0.38975904,  0.39698795,
				    0.40421687,  0.41144578,  0.4186747 ,  0.42590361,  0.43313253,
				    0.44036145,  0.44759036,  0.45481928,  0.46204819,  0.46927711,
				    0.47975904,  0.49060241,  0.50096386,  0.50819277,  0.51542169,
				    0.5226506 ,  0.52987952,  0.53710843,  0.54433735,  0.55156627,
				    0.55879518,  0.5660241 ,  0.57325301,  0.58048193,  0.58771084,
				    0.59493976,  0.60216867,  0.60939759,  0.61662651,  0.62578313,
				    0.63662651,  0.64746988,  0.65554217,  0.66277108,  0.67     };

/* ========================================================================== */
/* Variables read from a parameter file */
static Real mu=0.62, mue=1.18, muH = 1.4285714285714286;
static Real hubble=0.7;
static int cooling=0;
static int heating=0;
static int BCG_potential=0;
static int AGN_mode=0;
static int bubble_mode=0;
static int quasar_mode=0;
//static Real Tcool, Tfloor, Tceil, Tacc, dfloor, dceil, vceil;
static Real Tacc;
static Real f_rcf, alpha, beta;
static Real ep_fb,ep_b, f_gal, f_outflow_out, f_outflow_in, f_th, eta;
static Real OpeningSolidAngle; 
static Real P_jet, v_jet, theta_jet, t_AGN, dt_AGN, r_AGN, dt_jet_prec, eps_AGN;
static Real f_power, dt_power, phase_power, r_power, Edotcool_central, bubble_height;
static Real theta_quasar, f_quasar, macc_quasar, t_ff;
static Real M200, c_nfw, r_s, rho_s, r_0, rho_0, K0, Kindex, P0, T0, v_rot, M_BCG_4;
static Real r_switch, f_switch;
static Real delta_shell;
static int ind_switch;
static int nlevels;
static Real phi_bubble, theta_bubble, r_bubble;
static Real Bx, By, Bz;
long int iseed = -438573921;

/* end prototypes and definitions */
/* ========================================================================== */


/* ========================================================================== */
/* Create the initial condition and start the simulation! */
void problem(DomainS *pDomain)
{
  GridS *pGrid = pDomain->Grid;
  int i, j, k;
  int is,ie,js,je,ks,ke;
  int ind;

  Real x1,x2,x3,r,sinangle,cosangle;
  Real rho, P, rho_in, P_in, rho_out, P_out, x_switch;

  int n_entries = 200000;
  Real x_init[200000], rho_init[200000];

  Real r_01, rho_01, P_01, t_cool_01, Eth_int_01, Edot_th_int_01;
  Real lam1, lam2, lam3, lam4, dr;

  Real index_r, dx, dx1, dx2, dx3;

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;

  dx1 = pGrid->dx1;
  dx2 = pGrid->dx2;
  dx3 = pGrid->dx3;
  dx = MIN(MIN(dx1,dx2),dx3);

  /* Initialize variables */
  set_vars();

  ath_pout(0,"[problem]: M200 (Msun), c200, r_s (kpc), rho_s (cgs), r_0 (kpc), K0 (keV*cm^2), Kindex, rho_0 (cgs), T0 (K), M_BCG_4 \n %e, %e, %e, %e, %e, %e, %e, %e, %e, %e \n", \
	   M200,c_nfw,r_s/3.08e21,rho_s,r_0/3.08e21,K0/1.60218e-9,Kindex,rho_0,T0,M_BCG_4);

  /* Array of initial densities to interpolate from */
  RK4_dens(x_init,rho_init,n_entries);

  /* Set smoothing parameters */
  x_switch = r_switch;
  ind = ind_switch;

  rho_out = interpolate(x_init, rho_init, x_switch, n_entries); // Numerical solution assuming K(x)=K0*(1+x^Kindex)
  P_out = P0*(1.0+pow(x_switch,Kindex))*pow(rho_out,Gamma)/2.0; // code units 
  Tigm = P_out/rho_out;

  /* initialize gas variables on the grid */
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1 + x2*x2 + x3*x3); // in code units, i.e. units of r_0

	rho_in = interpolate(x_init, rho_init, r, n_entries);
        P_in   = P0*(1.0+pow(r,Kindex))*pow(rho_in,Gamma)/2.0;

	P = P_in; //pow(fabs(P_in), ind) + pow(fabs(P_out), ind);
	//P = pow(P, 1.0/ind);
	rho = rho_in; //pow(fabs(rho_in), ind) + pow(fabs(rho_out), ind);
        //rho = pow(rho, 1.0/ind);

	/* no rotation is assumed */

        pGrid->U[k][j][i].d = rho;
	cosangle = x1/r;
	sinangle = x2/r;
        pGrid->U[k][j][i].M1 = -rho*sinangle*v_rot;
        pGrid->U[k][j][i].M2 = rho*cosangle*v_rot;
        pGrid->U[k][j][i].M3 = 0.0;

#ifndef ISOTHERMAL
        pGrid->U[k][j][i].E = P/Gamma_1;
#endif  /* ISOTHERMAL */

#ifdef MHD
	pGrid->U[k][j][i].B1c = Bx;
        pGrid->U[k][j][i].B2c = By;
	pGrid->U[k][j][i].B3c = Bz;
#endif /* MHD */

#if (NSCALARS > 0)
#ifdef EXPLICIT_HEATING
	pGrid->U[k][j][i].s[0] = P/pow(rho,Gamma_1); // Conserved entropy field 
	pGrid->U[k][j][i].s[1] = 0.0; // Heating field
	pGrid->U[k][j][i].s[2] = 0.0; // Cooling field
#endif
#if (NSCALARS > 3)
	pGrid->U[k][j][i].s[3] = 0.0; // jet passive tracer
	index_r = floorf(r/(3*delta_shell)); // radius in unit of a macro-interval of thickness 3*delta_shell 
	// each interval is divided in 3 sub-intervals of thickness delta_shell (or 2*dx in the coarse region
	// only the central sub-interval is populated with the scalar
	if ( 3.0*index_r+2.0 <= r/delta_shell && r/delta_shell < 3.0*index_r+2.0+ MAX(1.0,2.0*dx/delta_shell) ) { 
	  pGrid->U[k][j][i].s[4] = rho;
	} else {
	  pGrid->U[k][j][i].s[4] = 0.0;
	}
#endif
#endif /* PASSIVELY ADVECTED SCALARS*/
      }
    }
  }

  /* Enforce temperature floor and density ceiling */ 
  enforce_floor(pDomain);

  /* Compute cooling rate within r_0 */
  if (AGN_mode > 0) {
    r_01 = 1.0; //0.1*c_nfw*r_s/r_0;
    rho_01 = interpolate(x_init, rho_init, r_01, n_entries);
    P_01 = P0*(1.0+pow(r_01,Kindex))*pow(rho_01,Gamma)/2.0;
    Eth_int_01 = 0.0;
    Edot_th_int_01 = 0.0;
    dr = x_init[1]-x_init[0];
    i=0;
    while (x_init[i] <= r_01) {
      P_in = P0*(1.0+pow(x_init[i],Kindex))*pow(rho_init[i],Gamma)/2.0;
      Eth_int_01 += 4.0*PI*x_init[i]*x_init[i]*dr*P_in/Gamma_1;
      lam1 = cool_only(rho_01,P_01);
      lam2 = cool_only(rho_01,P_01-(Gamma_1*(lam1/2.)));
      lam3 = cool_only(rho_01,P_01-(Gamma_1*(lam2/2.)));
      lam4 = cool_only(rho_01,P_01-(Gamma_1*lam3));
      Edot_th_int_01 += 4.0*PI*x_init[i]*x_init[i]*dr*(lam1 + 2.*lam2 + 2.*lam3 + lam4)/6.;
      i++;
    }
    t_cool_01 = Eth_int_01/Edot_th_int_01;
    ath_pout(0,"Eth(< r_0) = %e t_cool(r_0) = %e P_jet = %e P_jet/Edot_cool(< r_0) = %e \n", \
	     Eth_int_01, t_cool_01, P_jet, P_jet*t_cool_01/Eth_int_01);
  }

  return;
}

/* concentration of the NFW profile */
static Real set_c_nfw()
{
  Real c = 8.03*pow((M200*0.7/1.0e12),-0.101);
  return c;
}

/* scale density of associated to NFW potential in cgs */
static Real set_rho_s()
{
  Real rho_scale, mfactor, r200, rho_crit, H0;

  mfactor = log(1.0+c_nfw)-c_nfw/(1.0+c_nfw);
  H0 = 100.*hubble*1.0e5/3.08e24;
  rho_crit = 3.0*pow(H0,2.0)/(8.0*PI*6.67e-8);
  r200 = pow( 3.0*M200*2.0e33/(4.0*PI*200.*rho_crit) , 1.0/3.0);
  rho_scale = pow(c_nfw,3.0)*M200*2.0e33/(4.0*PI*mfactor*pow(r200,3.0));

  return rho_scale;
}

/* scale radius in cgs */
static Real set_rs()
{
  Real rs, r200, rho_crit, H0;
  H0 = 100.*hubble*1.0e5/3.08e24;
  rho_crit = 3.0*pow(H0,2.0)/(8.0*PI*6.67e-8);
  r200 = pow( 3.0*M200*2.0e33/(4.0*PI*200.*rho_crit) , 1.0/3.0);
  rs = r200/c_nfw;

  return rs;
}

/* pressure of gas at r_0 in code units */
static Real set_P0()
{
  Real P_zero,F;

  F = pow(1.66e-24,Gamma)*mu*pow(mue,Gamma_1);
  P_zero = K0/(4.0*PI*6.67e-8*F*pow(r_0,2.0)*pow(rho_0,2.0-Gamma)); 

  return P_zero;
}

/* central temperature of gas in K */
static Real set_T0()
{ 
  Real conversion, T_zero;
  
  conversion = mu*1.66e-24*4.0*PI*6.67e-8*rho_0*pow(r_0,2.0)/1.38e-16;
  T_zero = conversion*P0;  // density at r_0 is 1 in code units

  return T_zero; // in K
}

/* density derivative in the ICs in code units */
static Real drhodlogx(const Real rho, const Real xx, const Real hh)
{
  Real x, pre1, grad_phi, b, F, term1, term2, derivative;
  Real x1, x2, x3, xin, xout;

  x = exp(xx);
  b = r_0/r_s;
  xin = exp(xx-hh/100.)/pow(3,0.5);
  xout = exp(xx+hh/100.)/pow(3,0.5);
  F = pow(1.66e-24,Gamma)*mu*pow(mue,Gamma_1);
  //pre1 = 8.0*PI*6.67e-8*r_s*r_0*rho_s*F*pow(rho_0,1.0-Gamma)/(Gamma*K0)*pow(rho,2.0-Gamma)/(1.0+pow(x,Kindex));
  pre1 = 8.0*PI*6.67e-8*pow(r_0,2.0)*F*pow(rho_0,2.0-Gamma)/(Gamma*K0)*pow(rho,2.0-Gamma)/(1.0+pow(x,Kindex));
  grad_phi = -(StaticGravPot(xout,xout,xout)-StaticGravPot(xin,xin,xin))/(xout-xin);
  term1 = pre1 * grad_phi; //( 1.0/(1.0+b*x)/(b*x) - log(1.0+b*x)/pow(b*x,2.0) );
  term2 = -Kindex*rho*pow(x,Kindex-1.0)/(Gamma*(1.0+pow(x,Kindex)));

  derivative = x*(term1 + term2);

  return derivative;
}

/* 4th order Runge-Kutta to integrate density */
void RK4_dens(Real *x, Real *rho, const int n_entries)
{
  // Integrate from xmin to xmax at n_entries locations
  int i,ind; 
  Real xmin,xmax,h; // this is the RK step size 
  Real x_current, rho_new, rho_current, k1, k2, k3, k4;

  xmax = log(2.0*c_nfw*r_s/r_0); // maximum radius in the array 
  xmin = log(2.0*3.08e18/r_0); // minimum radius is 2 pc
  h = (xmax-xmin)/n_entries;

  // Identify location of r_0 in the x array and interpolate to cell center
  ind=(int)((0.0-xmin)/h); // log10(r_0)=log10(1)=0.0 in code units
  x[ind]=xmin+((Real)(ind)+0.5)*h;
  rho[ind]=1.0+(x[ind]-0.0)*drhodlogx(1.0,0.0,h);

  // Integrate outwards
  x_current = x[ind];
  rho_current = rho[ind]; 
  for (i=ind+1; i<n_entries; i++) {
    k1 = drhodlogx(rho_current,x_current,h);
    k2 = drhodlogx(rho_current+k1*h/2.0,x_current+h/2.0,h/2.0);
    k3 = drhodlogx(rho_current+k2*h/2.0,x_current+h/2.0,h/2.0);
    k4 = drhodlogx(rho_current+k3*h,x_current+h,h);
    rho_new = rho_current + (k1+2.0*k2+2.0*k3+k4)*h/6.0;
    x_current = x_current + h; 
    rho_current = rho_new;
    x[i]=x_current;
    rho[i]=rho_new;
  }

  // Integrate inwards
  x_current = x[ind];
  rho_current = rho[ind];
  for(i=ind-1; i>=0; i--) {
    k1 = drhodlogx(rho_current,x_current,h);
    k2 = drhodlogx(rho_current-k1*h/2.0,x_current-h/2.0,h/2.0);
    k3 = drhodlogx(rho_current-k2*h/2.0,x_current-h/2.0,h/2.0);
    k4 = drhodlogx(rho_current-k3*h,x_current-h,h);
    rho_new = rho_current - (k1+2.0*k2+2.0*k3+k4)*h/6.0;
    x_current = x_current - h;
    rho_current = rho_new;
    x[i]=x_current;
    rho[i]=rho_new;
  }

  for(i=0; i<n_entries; i++){
    x[i] = exp(x[i]);
  //  ath_pout(0,"x, rho, %e %e \n",x[i],rho[i]);
  }

  return;
}

/* use in problem() and read_restart()*/
static void set_vars()
{
  char *atm_file;
  Real dx, dxmin;
#ifdef STATIC_MESH_REFINEMENT
  int ir, irefine, nlevels;

#ifdef MPI_PARALLEL
  Real my_dxmin;
  int ierr;
#endif  /* MPI_PARALLEL */
#endif  /* STATIC_MESH_REFINEMENT */

  int iseed;

  iseed = -10;
#ifdef MPI_PARALLEL
  iseed -= myID_Comm_world;
#endif  /* MPI_PARALLEL */
  srand(iseed);

  /* Initialize parameters of the model */
  r_switch = par_getd_def("problem","r_switch",1.0);
  ind_switch = par_geti_def("problem","ind_switch",15);
  M200 = par_getd_def("problem","M200",1.0e14); // halo mass
  r_0 = par_getd_def("problem","r_0",3.0e1); // entropy core radius in kpc
  r_0 = r_0*3.08e21; // conversion to cm
  delta_shell = par_getd_def("problem","delta_shell",6.0); // thickness of mixing tracer shells in kpc
  delta_shell = delta_shell*3.08e21/r_0; // convert to code units
  rho_0 = par_getd_def("problem","rho_0",1.0e-25); // gas density at r_0 in cgs
  K0 = par_getd_def("problem","K0",3.0e1); // central entropy in keV*cm^2
  K0 = K0*1.60218e-9; // conversion to erg*cm^2
  Kindex = par_getd_def("problem","Kindex",1.5); // slope of the outer entropy profile
  v_rot = par_getd_def("problem","v_rot",0.0); // rotation velocity of the cluster in km/s
  v_rot = v_rot*1e5/r_0/pow(4.0*PI*6.67e-8*rho_0,0.5); // code units
  M_BCG_4 = par_getd_def("problem","M_BCG_4",0.0); // Mass of BCG within 4 kpc in solar masses
  c_nfw = set_c_nfw(); // halo concentration
  rho_s = set_rho_s(); // NFW scale density cgs
  r_s = set_rs(); // NFW scale radius cgs
  P0 = set_P0(); // pressure at r_0 in code units
  T0 = set_T0(); // temperature at r_0 in K
  //T0 = T0*8.6173324011e-08; // conversion to keV
  Bx = par_getd_def("problem","Bx",0.0); // B in microgauss
  Bx = Bx*1.0e-6*pow(rho_0,0.5)*r_0*pow(4.0*PI*6.67e-8*rho_0,0.5);
  By = par_getd_def("problem","By",0.0); // B in microgauss
  By = By*1.0e-6*pow(rho_0,0.5)*r_0*pow(4.0*PI*6.67e-8*rho_0,0.5);
  Bz = par_getd_def("problem","Bz",0.0); // B in microgauss
  Bz = Bz*1.0e-6*pow(rho_0,0.5)*r_0*pow(4.0*PI*6.67e-8*rho_0,0.5);

  /* Initialize cooling, heating and numerical variables */
  Tcool = par_getd_def("problem", "Tcool", 1.0e4); // cooling floor in K
  Tcool = Tcool/(4.0*PI*6.67e-8*rho_0*pow(r_0,2.0)*mu*1.66e-24/1.38e-16); // convert to P/rho code units 
  Tfloor = par_getd_def("problem","Tfloor",1.0e4); // temperature floor in K
  Tfloor = Tfloor/(4.0*PI*6.67e-8*rho_0*pow(r_0,2.0)*mu*1.66e-24/1.38e-16); // convert to P/rho code units 
  Tceil = par_getd_def("problem","Tceil",1.0e9); // temperature ceiling in K
  Tceil = Tceil/(4.0*PI*6.67e-8*rho_0*pow(r_0,2.0)*mu*1.66e-24/1.38e-16); // convert to P/rho code units
  Tacc = par_getd_def("problem","Tacc",3.0e4); // temperature below which accretion is considered in K
  Tacc = Tacc/(4.0*PI*6.67e-8*rho_0*pow(r_0,2.0)*mu*1.66e-24/1.38e-16); // convert to P/rho code units  
  dfloor = par_getd_def("problem", "dfloor",1.0e-10); // density floor in code units 
  dceil = par_getd_def("problem","dceil",1.0e10); // ceiling density in code units
  vceil =  par_getd_def("problem","vceil",3.0e4); // velocity ceiling in km/s
  vceil = vceil*1e5/r_0/pow(4.0*PI*6.67e-8*rho_0,0.5); // code units 
  cooling = par_geti_def("problem", "cooling", 0); // Is cooling on?
  heating = par_geti_def("problem", "heating", 0); // Is heating (Fielding et al. 2016) on?
  BCG_potential = par_geti_def("problem", "BCG_potential", 0); // Is the BCG potential on?

  f_rcf               = par_getd_def("problem", "f_rcf", 1.0); // used for simple cooling/heating
  alpha               = par_getd_def("problem", "alpha", -1.5); // used for simple cooling/heating
  beta                = par_getd_def("problem", "beta", 3.5); // used for simple cooling/heating

  /* AGN feedback parameters */
  AGN_mode = par_geti_def("problem", "AGN_mode", 0); // By default AGN feedback is off
  bubble_mode = par_geti_def("problem", "bubble_mode", 0);  // By default a jet is used; bubble_mode=1 activates thermalized bubbles
  quasar_mode = par_geti_def("problem", "quasar_mode", 0); // Active only for AGN_mode=4 (cold accretion) uses a quasi-spherical quasar mode 
  P_jet = par_getd_def("problem","P_jet",1.0e44); // AGN jet power in erg/s for AGN_mode=1 (fixed power), 2 (sinusoidal power)
  P_jet = P_jet*pow(4.0*PI*6.67e-8*rho_0,-1.5)/(rho_0*pow(r_0,5.0)); // conversion to code units
  v_jet = par_getd_def("problem","v_jet",1.0e4); // jet velocity
  v_jet = v_jet*1e5/r_0/pow(4.0*PI*6.67e-8*rho_0,0.5); // code units
  t_AGN = par_getd_def("problem","t_AGN",0.0); // time at which AGN is turned on [Myr]
  t_AGN = t_AGN*1e6*365*24*3600/pow(4.0*PI*6.67e-8*rho_0,-0.5); // in code units 
  dt_AGN = par_getd_def("problem","dt_AGN",100.0); // time interval in which the AGN is on [Myr]
  dt_AGN = dt_AGN*1e6*365*24*3600/pow(4.0*PI*6.67e-8*rho_0,-0.5); // in code units
  dt_jet_prec = par_getd_def("problem","dt_jet_prec",10.0); // jet precession timescale [Myr]
  dt_jet_prec = dt_jet_prec*1e6*365*24*3600/pow(4.0*PI*6.67e-8*rho_0,-0.5); // in code units
  theta_jet = par_getd_def("problem","theta_jet",5.0); // inclination of the jet in degrees
  theta_jet = theta_jet*PI/180; // conversion to radians 
  r_AGN = par_getd_def("problem","r_AGN",16.0); // injection radius in kpc
  r_AGN = r_AGN*3.08e21/r_0; // code units
  f_power = par_getd_def("problem","f_power",0.0); // relative amplitude of jet powr fluctuation wrt P_jet, AGN_mode=2
  dt_power = par_getd_def("problem","dt_power",50.0); // period of the jet power fluctuations [Myr], AGN_mode=2
  dt_power = dt_power*1e6*365*24*3600/pow(4.0*PI*6.67e-8*rho_0,-0.5); // in code units, AGN_mode=2
  phase_power = par_getd_def("problem","phase_power",0.0); // phase of the jet oscillations [rad], AGN_mode=2
  r_power = par_getd_def("problem","r_power",50.0); // radius for cooling rate, AGN_mode=3
  r_power = r_power*3.08e21/r_0; // code units 
  bubble_height = par_getd_def("problem","bubble_height",20.0); // Distance of the bubbles from the center in kpc, bubble_mode = 1
  bubble_height = bubble_height*3.08e21/r_0; 
  eps_AGN = par_getd_def("problem","eps_AGN",0.001); // feedback efficiency for AGN_mode = 4 (cold gas accretion)
  theta_quasar = par_getd_def("problem", "theta_quasar", 45.0); // Opening angle of the quasar wind
  theta_quasar = theta_quasar*PI/180; // conversion to radians
  f_quasar = par_getd_def("problem", "f_quasar", 0.0); // fraction of the power that goes into quasar mode
  macc_quasar =par_getd_def("problem", "macc_quasar", 0.0); // accretion rate threshold for triggering of quasar mode
  macc_quasar =macc_quasar*2e33/(365.*24*3600.)/(rho_0*pow(4.0*PI*6.67e-8*rho_0,0.5)*pow(r_0,3.0)); // to code units 
  t_ff = par_getd_def("problem", "t_ff", 5.0); // Free-fall time in Myr
  t_ff = t_ff*1e6*365*24*3600/pow(4.0*PI*6.67e-8*rho_0,-0.5); // in code units
#ifdef THERMAL_CONDUCTION
  kappa_iso = par_getd_def("problem","kappa_iso",0.0); // Isotrpic thermal diffusivity in cm2/s
  kappa_iso = kappa_iso/(pow(4.0*PI*6.67e-8*rho_0,0.5)*pow(r_0,2.0)); // Convert to dimensionless units
#endif

  /* Parameters in Drummond's version. Might be useful in the future */
  ep_fb               = par_getd_def("problem", "ep_fb", 0.0);
  ep_b                = par_getd_def("problem", "ep_b", 0.0);
  f_gal               = par_getd_def("problem", "f_gal", 0.1);
  f_outflow_in        = par_getd_def("problem", "f_outflow_in",  0.11 );
  f_outflow_out       = par_getd_def("problem", "f_outflow_out", 0.165);
  f_th                = par_getd_def("problem", "f_th", 0.1);
  eta                 = par_getd_def("problem", "eta", 5.0);
  OpeningSolidAngle   = par_getd_def("problem", "OpeningSolidAngle", PI);

  /* Gravitational potential */
  StaticGravPot = grav_pot;
  
  /* calculate dxmin.  this is pretty bad code... */
  dx = par_getd("domain1", "x1max")-par_getd("domain1", "x1min");
  dx /= (Real) par_geti("domain1", "Nx1");
  dxmin = dx;

  if (par_geti("domain1", "Nx2")>1) {
    dx = (par_getd("domain1", "x2max")-par_getd("domain1", "x2min"));
    dx /= (Real) par_geti("domain1", "Nx2");
    dxmin = MIN(dxmin, dx);
  }

  if (par_geti("domain1", "Nx3")>1) {
    dx = (par_getd("domain1", "x3max")-par_getd("domain1", "x3min"));
    dx /= (Real) par_geti("domain1", "Nx3");
    dxmin = MIN(dxmin, dx);
  }

  nlevels = par_geti("job", "num_domains");

#ifdef STATIC_MESH_REFINEMENT
  irefine = 1;
  nlevels = par_geti("job", "num_domains");
  for (ir=1; ir<nlevels; ir++) irefine *= 2;
  dxmin /= (Real) irefine;

  /* sync over all processors */
#ifdef MPI_PARALLEL
  my_dxmin = dxmin;
  ierr = MPI_Allreduce(&my_dxmin, &dxmin, 1,
                       MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
#endif /* MPI_PARALLEL */
#endif  /* STATIC_MESH_REFINEMENT */


  return;
}
/* end problem() */
/*=========================================================================== */


/* ========================================================================== */
/* ========================================================================== */
/* ========================          Cooling          ======================= */
/* ========================================================================== */
/* ========================================================================== */

/* Given P and d in code units, return kT in keV. */
static Real kT_keV(const Real P, const Real d)
{
  return(P/d*8.6173324011e-08*4.0*PI*6.67e-8*rho_0*pow(r_0,2.0)*mu*1.66e-24/1.38e-16);
}

static Real kT_keV_inverse(const Real T)
{
  return(T/(8.6173324011e-08*4.0*PI*6.67e-8*rho_0*pow(r_0,2.0)*mu*1.66e-24/1.38e-16));
}

static Real L_23(const Real T)
{
  if (T <= kT_keV(Tcool, 1.0)) { //no cooling below Tcool
    return 0.0;
  } else {
    if (T >= 27.2503977422 ){ // powerlaw fit above log10(T) = 8.5
      return 4.6773514128720004 * pow(T/27.2503977422, 0.44);
    } else if (T <= 1.2172305551e-3) { // powerlaw fit below log10(T) = 4.15
      return 2.75422850468683*pow(T/1.2172305551e-3, 21.8);
    } else { // interpolate in between 
      return pow(10,interpolate(logkT_SD, logLambda_23_SD, log10(T), 250));
    }
  }
}

static Real cool_only(const Real d, const Real P)
{
  /*
  Calculate Edot where positive means remove energy
  */
  Real kT, Edot;
  kT = kT_keV(P, d);
  Edot = pow(d/(mue*1.66e-24*r_0),2.0) / pow(4.0*PI*6.67e-8,1.5) / pow(rho_0,0.5) * 1.0e-23 * L_23(kT);
  return Edot; // code units
}

static Real coolheat_simple(const Real d, const Real P, const Real r, const Real dM,const Real dt)
{
  /*
  Calculate Edot where positive means remove energy
  */
  Real kT, Edot_cool,Edot_heat, Edot;
  Real rcf,c,Kn;
  Kn = (beta/PI)*sin(((3+alpha)/beta)*PI);
  rcf = f_rcf*c_nfw*r_s/r_0; // a fraction of R200 in code units 
  c = 3.0e10/(r_0*pow(4.0*PI*6.67e-8*rho_0,0.5)); // speed of light in code units
  kT = kT_keV(P, d); // temperature in keV
  Edot_cool = pow(d/(mue*1.66e-24*r_0),2.0) / pow(4.0*PI*6.67e-8,1.5) / pow(rho_0,0.5) * 1.0e-23 * L_23(kT); // code units 
  Edot_heat =  (ep_fb*dM/dt*c*c);
  Edot_heat *= Kn/(4*PI*pow(rcf,3));
  Edot_heat *= pow(r/rcf,alpha)/(1+pow(r/rcf,beta));
  Edot = (Edot_cool - Edot_heat);
  return Edot;
}

static Real coolheat_density(const Real d, const Real P, const Real r, const Real avgd, const Real dM,const Real dt)
{  
  /*
  Calculate Edot where positive means remove energy
  heating : H(r,) = H(r) /<(r)>
  */
  Real kT, Edot_cool,Edot_heat, Edot;
  Real rcf,c,Kn;
  Kn = (beta/PI)*sin(((3+alpha)/beta)*PI);
  rcf = f_rcf*c_nfw*r_s/r_0; // a fraction of R200 in code units
  c = 3.0e10/(r_0*pow(4.0*PI*6.67e-8*rho_0,0.5)); // speed of light in code units
  kT = kT_keV(P, d); // temperature in keV
  Edot_cool =  pow(d/(mue*1.66e-24*r_0),2.0) / pow(4.0*PI*6.67e-8,1.5) / pow(rho_0,0.5) * 1.0e-23 * L_23(kT); // code units
  Edot_heat =  (ep_fb*dM/dt*c*c);
  Edot_heat *= Kn/(4*PI*pow(rcf,3));
  Edot_heat *= pow(r/rcf,alpha)/(1+pow(r/rcf,beta));
  Edot_heat *= MIN(d/avgd,100.);
  Edot = (Edot_cool - Edot_heat);
  return Edot;
}

/* ========================================================================== */
static void cool_step(DomainS *pDomain, const Real dt, const Real dM, const Real t_hydro, const int iter, const int n_sub, const int LevelMax)
{
/*
This is a 4th order Runge-Kutta explicit integration 
scheme for the radiative cooling. It should be identical
to that used by Sutherland, Bicknell, & Dopita (2003).
http://iopscience.iop.org/0004-637X/591/1/238/pdf/57453.web.pdf 
*/
  GridS *pGrid = pDomain->Grid;
  int i, j, k;
  Real KE, ME, d, P, T;
  Real lam1, lam2, lam3, lam4;
  int is,ie,js,je,ks,ke;
  Real x1,x2,x3,r;
  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;
  /* ---------------------------------- */
  /* heating == 2 : -dependent heating */
  /* ---------------------------------- */
  int N_bins = 20;
  int iBin,ierr;
  Real rmin = 0.05*c_nfw*r_s/r_0; // minimum radius for binning, set to 1/20th rvir = 1/2 rgal
  Real dlogr = log10(2.0*c_nfw*r_s/r_0/rmin)/(N_bins-1);
  Real my_TotDens[N_bins] ; Real TotDens[N_bins] ;
  Real my_NinBins[N_bins] ; Real NinBins[N_bins] ;
  Real avgd;
  Real Tmin;
  Tmin = Tfloor/(pow(2.0,nlevels-1.));
  if (heating == 2){
    for (iBin=0;iBin<N_bins;iBin++){
      my_TotDens[iBin] = 0.0;
      TotDens[iBin] = 0.0;
      my_NinBins[iBin] = 0.0;
      NinBins[iBin] = 0.0;
    }
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
          r = sqrt(x1*x1+x2*x2+x3*x3);
          iBin = MAX(ceil(log10(r/rmin) / dlogr),0);
          my_TotDens[iBin] += pGrid->U[k][j][i].d;
          my_NinBins[iBin] += 1.0;
        }
      }
    }
#ifdef MPI_PARALLEL 
    MPI_Barrier( pDomain->Comm_Domain ) ; 
    ierr = MPI_Allreduce(&my_TotDens, &TotDens, N_bins,
                         MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
    ierr = MPI_Allreduce(&my_NinBins, &NinBins, N_bins,
                         MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
#else
    TotDens = my_TotDens;
    NinBins = my_NinBins;
#endif /* MPI_PARALLEL */
  }

  /* --------------------------------- */
  /* heating == 3 : mechanical heating */
  /* --------------------------------- */
  Real CosOpeningAngle;
  Real my_GalMass    = 0.0; Real GalMass    = 0.0; 
  Real my_N_OutflowRegionCells = 0.0; Real N_OutflowRegionCells = 0.0;
  Real Volume_OutflowRegion;
  Real dV,c;
  if (heating == 3){// mechanical heating
    c = 3.0e10/(r_0*pow(4.0*PI*6.67e-8*rho_0,0.5)); // speed of light in code units
    dV = pGrid->dx1 * pGrid->dx2 * pGrid->dx3;
    CosOpeningAngle = (1-OpeningSolidAngle/(2*PI));
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
        for (i=is; i<=ie; i++) {
          cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
          r = sqrt(x1*x1+x2*x2+x3*x3);
          if (r <= f_gal*c_nfw*r_s/r_0){
            my_GalMass += pGrid->U[k][j][i].d;
          }
          if (fabs(x3/r) >= CosOpeningAngle && r <= f_outflow_out*c_nfw*r_s/r_0 && r > f_outflow_in*c_nfw*r_s/r_0){
            KE = (SQR(pGrid->U[k][j][i].M1) +
                  SQR(pGrid->U[k][j][i].M2) +
                  SQR(pGrid->U[k][j][i].M3)) / (2.0 * pGrid->U[k][j][i].d);
            ME = 0.0;
#ifdef MHD
            ME = (SQR(pGrid->U[k][j][i].B1c) +
                  SQR(pGrid->U[k][j][i].B2c) +
                  SQR(pGrid->U[k][j][i].B3c)) * 0.5;
#endif  /* MHD */
            my_N_OutflowRegionCells += 1.0;
          }
        }
      }
    }
#ifdef MPI_PARALLEL 
    // ierr = MPI_Allreduce(&my_GalMass, &GalMass, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    // ierr = MPI_Allreduce(&my_N_OutflowRegionCells, &N_OutflowRegionCells, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    ierr = MPI_Allreduce(&my_GalMass, &GalMass, 1, MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
    ierr = MPI_Allreduce(&my_N_OutflowRegionCells, &N_OutflowRegionCells, 1, MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
#else
    GalMass               = my_GalMass;
    N_OutflowRegionCells  = my_N_OutflowRegionCells;
#endif /* MPI_PARALLEL */
    GalMass               = GalMass*dV;
    Volume_OutflowRegion  = N_OutflowRegionCells*dV;
    ath_pout(0, "[heating] GalMass = %e, dM = %e \n", GalMass, dM);
    // ath_pout(-1, "[heating] level = %i, GalMass = %e, dM = %e \n", pDomain->Level, GalMass, dM);
  }
  
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1+x2*x2+x3*x3);
        if (r <= 2.0*c_nfw*r_s/r_0) {
          d = pGrid->U[k][j][i].d;
          KE = (SQR(pGrid->U[k][j][i].M1) +
                SQR(pGrid->U[k][j][i].M2) +
                SQR(pGrid->U[k][j][i].M3)) / (2.0 * d);
          ME = 0.0;
#ifdef MHD
          ME = (SQR(pGrid->U[k][j][i].B1c) +
                SQR(pGrid->U[k][j][i].B2c) +
                SQR(pGrid->U[k][j][i].B3c)) * 0.5;
#endif  /* MHD */
          P = (pGrid->U[k][j][i].E - ME - KE) * Gamma_1;
          /* ---------------------------------- */
          /* ------     cooling only     ------ */
          /* ---------------------------------- */
          if (heating == 0 && bubble_mode == 0){
            lam1 = cool_only(d,P)*dt;
            lam2 = cool_only(d,P-(Gamma_1*(lam1/2.)))*dt;
            lam3 = cool_only(d,P-(Gamma_1*(lam2/2.)))*dt;
            lam4 = cool_only(d,P-(Gamma_1*lam3))*dt;
            pGrid->U[k][j][i].E -= (lam1 + 2.*lam2 + 2.*lam3 + lam4)/6.; 
#if (NSCALARS >0)
#ifdef EXPLICIT_HEATING /* store the cooling losses in a scalar field */
	    if (iter == 0) { 
              pGrid->U[k][j][i].s[1] = pGrid->U[k][j][i].s[1]/(n_sub*dt);
	      pGrid->U[k][j][i].s[2] = 0.0; }
	    pGrid->U[k][j][i].s[2] += (lam1 + 2.*lam2 + 2.*lam3 + lam4)/6./(n_sub*dt);
#endif
#endif
          }
          /* ---------------------------------- */
          /* ---  rho-independent heating   --- */
          /* ---------------------------------- */
          if (heating == 1){
            lam1 = coolheat_simple(d,P,r,dM,dt)*dt;
            lam2 = coolheat_simple(d,P-(Gamma_1*(lam1/2.)),r,dM,dt)*dt;
            lam3 = coolheat_simple(d,P-(Gamma_1*(lam2/2.)),r,dM,dt)*dt;
            lam4 = coolheat_simple(d,P-(Gamma_1*lam3),r,dM,dt)*dt;
            pGrid->U[k][j][i].E -= (lam1 + 2.*lam2 + 2.*lam3 + lam4)/6.;
            T = Gamma_1 * (pGrid->U[k][j][i].E - KE - ME) / d;
            T = MAX(T, Tigm);
            pGrid->U[k][j][i].E = d*T/Gamma_1 + KE + ME;
	  }
          /* ---------------------------------- */
          /* ---  rho-dependent heating     --- */
          /* ---------------------------------- */
          if (heating == 2){
            iBin = MAX(ceil(log10(r/rmin) / dlogr),0);
            avgd = TotDens[iBin]/NinBins[iBin];
            lam1 = coolheat_density(d,P                    ,r,avgd,dM,dt)*dt;
            lam2 = coolheat_density(d,P-(Gamma_1*(lam1/2.)),r,avgd,dM,dt)*dt;
            lam3 = coolheat_density(d,P-(Gamma_1*(lam2/2.)),r,avgd,dM,dt)*dt;
            lam4 = coolheat_density(d,P-(Gamma_1*lam3)     ,r,avgd,dM,dt)*dt;
            pGrid->U[k][j][i].E -= (lam1 + 2.*lam2 + 2.*lam3 + lam4)/6.;
            T = Gamma_1 * (pGrid->U[k][j][i].E - KE - ME) / d;
            T = MAX(T, Tigm);
            pGrid->U[k][j][i].E = d*T/Gamma_1 + KE + ME;
          }
          /* ---------------------------------- */
          /* ------  mechanical heating  ------ */
          /* ---------------------------------- */
          if (heating == 3){
            Real drho;
            Real dE;
            lam1 = cool_only(d,P)*dt;
            lam2 = cool_only(d,P-(Gamma_1*(lam1/2.)))*dt;
            lam3 = cool_only(d,P-(Gamma_1*(lam2/2.)))*dt;
            lam4 = cool_only(d,P-(Gamma_1*lam3))*dt;
            pGrid->U[k][j][i].E -= (lam1 + 2.*lam2 + 2.*lam3 + lam4)/6.;               // Remove energy due to cooling
            P = MAX(d*Tcool, (pGrid->U[k][j][i].E - ME - KE) * Gamma_1);               // find the new pressure at the new, lower T, but don't let T be less than Tcool
            if (r <= f_gal*c_nfw*r_s/r_0){
              P  *= (1. - dM / GalMass);                                // decrease P to preserve T
              pGrid->U[k][j][i].M1 *= (1. - dM / GalMass);              // decrease momentum to preserve vx
              pGrid->U[k][j][i].M2 *= (1. - dM / GalMass);              // decrease momentum to preserve vy
              pGrid->U[k][j][i].M3 *= (1. - dM / GalMass);              // decrease momentum to preserve vz
              pGrid->U[k][j][i].d  *= (1. - dM / GalMass);              // remove the mass
              // if ( ((pGrid->U[k][j][i].M1*x1)+(pGrid->U[k][j][i].M2*x2)+(pGrid->U[k][j][i].M3*x3)) > 0.0){
              //   pGrid->U[k][j][i].M1 = 0.0; pGrid->U[k][j][i].M2 = 0.0; pGrid->U[k][j][i].M3 = 0.0;
              // }
              KE = (SQR(pGrid->U[k][j][i].M1) +                                        // calculate new KE
                    SQR(pGrid->U[k][j][i].M2) +
                    SQR(pGrid->U[k][j][i].M3)) / (2.0 * pGrid->U[k][j][i].d);
              pGrid->U[k][j][i].E = P/Gamma_1 + ME + KE;
            }
            if (fabs(x3/r) >= CosOpeningAngle && r <= f_outflow_out*c_nfw*r_s/r_0 && r > f_outflow_in*c_nfw*r_s/r_0 && eta>0.0){
	      // Add in mass deltaM = Mdot_out deltat = (eta/(1+eta)) Mdot_in deltat
	      // Add in Kinetic Energy deltaKE = ep_fb Mdot_in c**2 (1-f_th) deltat
	      // Add in Thermal Energy deltaTE = ep_fb Mdot_in c**2 (f_th) deltat = drho kbT / Gamma_1 mu mp
              drho = (eta/(1.+eta))*dM/(Volume_OutflowRegion);                                  // add mass equally across all cells
              dE = 0.5*drho*(2.0*ep_b+1)*(-2.0*phi_nfw(r));
              // P  *=(1.+drho/pGrid->U[k][j][i].d);                                               // increase pressure to maintain T
              P  += dE * f_th * Gamma_1;                                                        // add to pressure for thermal energy contribution
	      pGrid->U[k][j][i].d  += drho;                                                     // increase the density 
              pGrid->U[k][j][i].M1 += drho*sqrt((1.0-f_th)*(2.0*ep_b+1)*(-2.0*phi_nfw(r))) * x1/r; // add the momentum to the cells given 
              pGrid->U[k][j][i].M2 += drho*sqrt((1.0-f_th)*(2.0*ep_b+1)*(-2.0*phi_nfw(r))) * x2/r; // the amount of KE they have received
              pGrid->U[k][j][i].M3 += drho*sqrt((1.0-f_th)*(2.0*ep_b+1)*(-2.0*phi_nfw(r))) * x3/r; // purely in the positive radial direction
              KE = (SQR(pGrid->U[k][j][i].M1) +                                                    // add to Kinetic Energy
                    SQR(pGrid->U[k][j][i].M2) +
                    SQR(pGrid->U[k][j][i].M3)) / (2.0 * pGrid->U[k][j][i].d);
              pGrid->U[k][j][i].E   = MAX(P, Tcool*pGrid->U[k][j][i].d)/Gamma_1 + ME + KE;                  // set the new total internal energy
#if (NSCALARS == 1)
	      pGrid->U[k][j][i].s[0] += drho;
#endif
	    }
	  }
	}
      }
    }
  }


  // Central sink and AGN feedback
  int ncells_AGN;
  Real P_temp;
  Real my_Msink, Msink;

  ncells_AGN = r_AGN/2.0/pGrid->dx1;

  // For all the cases in which Drummond's routines are not used, remove mass from the central sink
  if ( heating == 0 ) {
    my_Msink = 0.0; 
    Msink = 0.0;
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
	for (i=is; i<=ie; i++) {
	  cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
	  r = sqrt(x1*x1 + x2*x2 + x3*x3);
	  my_Msink += (r <= r_AGN) ? pGrid->U[k][j][i].d*pGrid->dx1*pGrid->dx2*pGrid->dx3 : 0.0;
	}
      }
    }
#ifdef MPI_PARALLEL
    ierr = MPI_Allreduce(&my_Msink, &Msink, 1,
			 MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
#else
    Msink = my_Msink;
#endif
    //if(Msink > 0.0) printf("Msink = %e, dM = %e \n",Msink,dM);    
    Msink = Msink + 1e-3*dfloor*pow(r_AGN,3.0);
    for (k=ks; k<=ke; k++) {
      for (j=js; j<=je; j++) {
	for (i=is; i<=ie; i++) {
          cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
          r = sqrt(x1*x1 + x2*x2 + x3*x3);
	  if (r <= r_AGN) {
	    P_temp = Gamma_1 * (pGrid->U[k][j][i].E -
				0.5*(SQR(pGrid->U[k][j][i].M1) +
				     SQR(pGrid->U[k][j][i].M2) +
				     SQR(pGrid->U[k][j][i].M3))/pGrid->U[k][j][i].d);
	    pGrid->U[k][j][i].d  = pGrid->U[k][j][i].d*MAX(0.1,(1.0-dM/Msink));
	    pGrid->U[k][j][i].M1 = pGrid->U[k][j][i].M1*MAX(0.1,(1.0-dM/Msink));
	    pGrid->U[k][j][i].M2 = pGrid->U[k][j][i].M2*MAX(0.1,(1.0-dM/Msink));
	    pGrid->U[k][j][i].M3 = pGrid->U[k][j][i].M3*MAX(0.1,(1.0-dM/Msink));
	    pGrid->U[k][j][i].E = P_temp*MAX(0.1,(1.0-dM/Msink))/Gamma_1 +
	      0.5*(SQR(pGrid->U[k][j][i].M1) +
		   SQR(pGrid->U[k][j][i].M2) +
		   SQR(pGrid->U[k][j][i].M3))/pGrid->U[k][j][i].d; 	  
	  }
	}
      }
    }
  }

  if ( AGN_mode > 0 ) {
    int n_cell_jet, my_n_cell_jet;
    Real dx1,dx2,dx3,dx,zz,rr;
    Real r_up, r_down, my_M_up, M_up, my_M_down, M_down, rho_b_up, rho_b_down;
    Real x_b_up, y_b_up, z_b_up, x_b_down, y_b_down, z_b_down;
    Real r_jet,E_jet_cell,Eth,P_jet_of_t,dP_jet,rho_jet;
    Real my_totW_jet,totW_jet;
    Real v_cell,v_x_jet,v_y_jet,v_z_jet;
    Real omega_jet_prec,fac_ang, fac_t;
    Real rho_quasar,E_quasar_cell,v_x_quasar,v_y_quasar,v_z_quasar;
    Real quasar_angle;
    Real c_light;

    dx1 = pGrid->dx1;
    dx2 = pGrid->dx2;
    dx3 = pGrid->dx3;
    dV = dx1*dx2*dx3;
    dx = MIN(dx1,dx2);

    // Check if it's the right time
    if (t_AGN <= t_hydro && t_hydro <= t_AGN+dt_AGN){
      // Injection in two parallel planes at z=+-dx
      // Each plane is a circle of radius 4 cells
      // Radial smoothing using a Gaussian or a 
      // Power law profile is implemented

      fac_t=MIN(1.0,(t_hydro-t_AGN)/dt_jet_prec);
      
      r_jet = 1.5*((float)ncells_AGN/2.0)*dx;

      // Determine number of cells in circle and cooling rate within r_0
      my_n_cell_jet=0;
      my_totW_jet = 0.0;
      my_M_up = 0.0;
      my_M_down = 0.0;
      for (k=ks; k<=ke; k++) {
        for (j=js; j<=je; j++) {
          for (i=is; i<=ie; i++) {
	    //fc_pos(pGrid,i,j,k,&x1,&x2,&x3);
            cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
	    if (bubble_mode == 0) {
	      r = sqrt(x1*x1 + x2*x2);
	      // jet model
	      //if ( ncells_AGN < (int)(x3/pGrid->dx3) && (int)(x3/pGrid->dx3) <= ncells_AGN+1 && \
	      //     r < r_AGN){
	      if ( 2 < (int)(x3/pGrid->dx3) && (int)(x3/pGrid->dx3) <= 3 && \
		   r < r_AGN/2.0 && pDomain->Level == LevelMax ){
		my_n_cell_jet = my_n_cell_jet + 1;
		// Gaussian smoothing
		my_totW_jet = my_totW_jet + exp(-0.5*pow(r/r_jet,2.0))*exp(-0.5*pow(x3/r_jet,2.0))*dV;
		if (pGrid->U[k][j][i].d != pGrid->U[k][j][i].d) {
		  printf(" %e %e %e %e %e %e %e \n",x1,x2,x3,pGrid->U[k][j][i].d,pGrid->U[k][j][i].M1,pGrid->U[k][j][i].M2,pGrid->U[k][j][i].M3);
		}
	      }
	    } else {
	      // bubble_mode

#if (NSCALARS >0)
#ifdef EXPLICIT_HEATING /* store the cooling losses in a scalar field */
              if (iter == 0) {
                pGrid->U[k][j][i].s[1] = pGrid->U[k][j][i].s[1]/(n_sub*dt);
                pGrid->U[k][j][i].s[2] = 0.0; }
#endif
#endif

              // Pure precession
              omega_jet_prec = 2.0*PI/dt_jet_prec;
              fac_ang = 1.0;
	     
              x_b_up = r_bubble*sin(theta_bubble*fac_ang)*cos(phi_bubble);
              y_b_up = r_bubble*sin(theta_bubble*fac_ang)*sin(phi_bubble);
              z_b_up = r_bubble*cos(theta_bubble*fac_ang);
	      x_b_down = -r_bubble*sin(theta_bubble*fac_ang)*cos(phi_bubble);
              y_b_down = -r_bubble*sin(theta_bubble*fac_ang)*sin(phi_bubble);
              z_b_down = -r_bubble*cos(theta_bubble*fac_ang);

              r_up = sqrt(SQR(x1-x_b_up) + SQR(x2-y_b_up) + SQR(x3-z_b_up));
              r_down = sqrt(SQR(x1-x_b_down) + SQR(x2-y_b_down) + SQR(x3-z_b_down));

	      if (r_up < r_AGN && pDomain->Level == LevelMax) {
		my_totW_jet = my_totW_jet + dV; // For the bubble this is just the volume
		my_M_up = my_M_up + dV*pGrid->U[k][j][i].d;
#if (NSCALARS >0)
#ifdef EXPLICIT_HEATING /* store the cooling losses in a scalar field */
                pGrid->U[k][j][i].s[2] = 0.0;
#endif
#endif
	      }
	      if (r_down < r_AGN && pDomain->Level == LevelMax) {
                my_M_down = my_M_down + dV*pGrid->U[k][j][i].d;
#if (NSCALARS >0)
#ifdef EXPLICIT_HEATING /* store the cooling losses in a scalar field */
		pGrid->U[k][j][i].s[2] = 0.0; 
#endif
#endif
              }
	      // cooling only outside the bubbles
	      if (r_down >= r_AGN && r_up >= r_AGN && pDomain->Level == LevelMax){
		d = pGrid->U[k][j][i].d;
		KE = (SQR(pGrid->U[k][j][i].M1) +
		      SQR(pGrid->U[k][j][i].M2) +
		      SQR(pGrid->U[k][j][i].M3)) / (2.0 * d);
		ME = 0.0;
#ifdef MHD
		ME = (SQR(pGrid->U[k][j][i].B1c) +
		      SQR(pGrid->U[k][j][i].B2c) +
		      SQR(pGrid->U[k][j][i].B3c)) * 0.5;
#endif  /* MHD */
		P = (pGrid->U[k][j][i].E - ME - KE) * Gamma_1;
		lam1 = cool_only(d,P)*dt;
		lam2 = cool_only(d,P-(Gamma_1*(lam1/2.)))*dt;
		lam3 = cool_only(d,P-(Gamma_1*(lam2/2.)))*dt;
		lam4 = cool_only(d,P-(Gamma_1*lam3))*dt;
		pGrid->U[k][j][i].E -= (lam1 + 2.*lam2 + 2.*lam3 + lam4)/6.;
#if (NSCALARS >0)
#ifdef EXPLICIT_HEATING /* store the cooling losses in a scalar field */
                pGrid->U[k][j][i].s[2] += (lam1 + 2.*lam2 + 2.*lam3 + lam4)/6./(n_sub*dt);
#endif
#endif
	      }
	    }
          }
        }
      }

      // Communication 
#ifdef MPI_PARALLEL
      MPI_Barrier( pDomain->Comm_Domain );
      ierr = MPI_Allreduce(&my_n_cell_jet, &n_cell_jet, 1, MPI_INT, MPI_SUM, pDomain->Comm_Domain);
      ierr = MPI_Allreduce(&my_totW_jet, &totW_jet, 1, MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
      ierr = MPI_Allreduce(&my_M_up, &M_up, 1, MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
      ierr = MPI_Allreduce(&my_M_down, &M_down, 1, MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
#else
      n_cell_jet = my_n_cell_jet;
      totW_jet = my_totW_jet;
      M_up = my_M_up;
      M_down = my_M_down;
#endif

      if (AGN_mode == 1) {
        P_jet_of_t = P_jet;
      }
      if (AGN_mode == 2) {
        P_jet_of_t = P_jet*(1.0 + f_power*sin(2.0*PI*(t_hydro-t_AGN)/dt_power+phase_power));
      }
      if (AGN_mode == 3) {
	P_jet_of_t = Edotcool_central;
      }
      if (AGN_mode >= 4) {
	c_light = 3.0e10/(r_0*pow(4.0*PI*6.67e-8*rho_0,0.5)); // speed of light in code units
	P_jet_of_t = eps_AGN*dM/dt*c_light*c_light;
      }
      

      ath_pout(0,"t = %e, AGN dM/dt = %e, P_jet = %e \n",t_hydro,dM/dt,P_jet_of_t);

      // Normalization of the jet power per unit volume
      dP_jet = P_jet_of_t/totW_jet;

      // boost factor for precession angle)
      fac_ang = 1.0; 

      // Begin loop on cells
      for (k=ks; k<=ke; k++) {
        for (j=js; j<=je; j++) {
          for (i=is; i<=ie; i++) {
            cc_pos(pGrid,i,j,k,&x1,&x2,&x3);

	    KE = (SQR(pGrid->U[k][j][i].M1) +
		  SQR(pGrid->U[k][j][i].M2) +
		  SQR(pGrid->U[k][j][i].M3)) / (2.0 * d);
	    ME = 0.0;
#ifdef MHD
	    ME = (SQR(pGrid->U[k][j][i].B1c) +
		  SQR(pGrid->U[k][j][i].B2c) +
		  SQR(pGrid->U[k][j][i].B3c)) * 0.5;
#endif  /* MHD */
	    P = (pGrid->U[k][j][i].E - ME - KE) * Gamma_1;

	    if (bubble_mode == 0) {
	      r = sqrt(x1*x1 + x2*x2);
	      rr = sqrt(x1*x1 + x2*x2 + x3*x3);

	      // Upward jet
	      //if ( ncells_AGN < (int)(x3/pGrid->dx3) && (int)(x3/pGrid->dx3) <= ncells_AGN+1 && \
	      //   r < r_AGN){
	      if ( 2 < (int)(x3/pGrid->dx3) && (int)(x3/pGrid->dx3) <= 3 && \
                   r < r_AGN/2.0 && pDomain->Level == LevelMax){
	      // Compute jet properties
		// Gaussian smoothing 
		E_jet_cell = fac_t*(1.0-f_th)*dP_jet*dt*exp(-0.5*pow(r/r_jet,2.0))*exp(-0.5*pow(x3/r_jet,2.0))/2.0; // Only half the energy upwards
                v_cell = v_jet;
		if (quasar_mode == 1 && AGN_mode < 4) {
		  E_quasar_cell = f_quasar*E_jet_cell; // power going into quasar mode
		  E_jet_cell = (1.0-f_quasar)*E_jet_cell; // remove the fraction of power that goes to quasar mode
		  rho_quasar = 2.0*E_quasar_cell/pow(v_cell,2.0);
		} else if (quasar_mode == 1 && AGN_mode >= 4 && dM/dt >= macc_quasar) {
                  E_quasar_cell = f_quasar*E_jet_cell; // power going into quasar mode
		  E_jet_cell = (1.0-f_quasar)*E_jet_cell; // remove the fraction of power that goes to quasar mode
                  rho_quasar = 2.0*E_quasar_cell/pow(v_cell,2.0);
		}
                rho_jet = 2.0*E_jet_cell/pow(v_cell,2.0);		

		// Pure precession
		omega_jet_prec = 2.0*PI/dt_jet_prec;
		v_x_jet = v_cell*sin(theta_jet*fac_ang)*cos(omega_jet_prec*(t_hydro-t_AGN));
		v_y_jet = v_cell*sin(theta_jet*fac_ang)*sin(omega_jet_prec*(t_hydro-t_AGN));
		v_z_jet = v_cell*cos(theta_jet*fac_ang);
		
		// update cons variables
		pGrid->U[k][j][i].d += rho_jet;
		pGrid->U[k][j][i].M1 += rho_jet*v_x_jet;
		pGrid->U[k][j][i].M2 += rho_jet*v_y_jet;
		pGrid->U[k][j][i].M3 += rho_jet*v_z_jet;
		// Gaussian
		Eth = fac_t*f_th*(1.0-f_quasar)*dP_jet*dt*exp(-0.5*pow(r/r_jet,2.0))*exp(-0.5*pow(x3/r_jet,2.0))/2.0;
		KE = 0.5*rho_jet*v_cell*v_cell;
		ME = 0.0;
		pGrid->U[k][j][i].E += Eth + ME + KE;
#if (NSCALARS > 3)
		pGrid->U[k][j][i].s[3] += rho_jet; // jet passive tracer
#endif

                // Inject quasar wind
                if (quasar_mode == 1) {
                  // wind angle
                  quasar_angle = pow(x1*x1+x2*x2,0.5)/r_AGN*theta_quasar;
                  v_x_quasar = v_cell*sin(quasar_angle)*x1/pow(x1*x1+x2*x2,0.5);
                  v_y_quasar = v_cell*sin(quasar_angle)*x2/pow(x1*x1+x2*x2,0.5);
                  v_z_quasar = v_cell*cos(quasar_angle);

                  // update cons variables
                  pGrid->U[k][j][i].d += rho_quasar;
                  pGrid->U[k][j][i].M1 += rho_quasar*v_x_quasar;
                  pGrid->U[k][j][i].M2 += rho_quasar*v_y_quasar;
                  pGrid->U[k][j][i].M3 += rho_quasar*v_z_quasar;
                  // Gaussian
                  Eth = fac_t*f_th*f_quasar*dP_jet*dt*exp(-0.5*pow(r/r_jet,2.0))*exp(-0.5*pow(x3/r_jet,2.0))/2.0;
                  KE = 0.5*rho_quasar*v_cell*v_cell;
                  ME = 0.0;
                  pGrid->U[k][j][i].E += Eth + ME + KE;
#if (NSCALARS > 3)
                  pGrid->U[k][j][i].s[3] += rho_quasar; // jet passive tracer
#endif
                }

	      }

	      // Downward jet
	      
	      //if ( -ncells_AGN-1 <= (int)(x3/pGrid->dx3) && (int)(x3/pGrid->dx3) < -ncells_AGN && \
	      //   r < r_AGN){
	      if ( -3 <= (int)(x3/pGrid->dx3) && (int)(x3/pGrid->dx3) < -2 && \
                   r < r_AGN/2.0 && pDomain->Level == LevelMax){
		// Compute jet properties
		// Gaussian smoothing
		E_jet_cell = fac_t*(1.0-f_th)*dP_jet*dt*exp(-0.5*pow(r/r_jet,2.0))*exp(-0.5*pow(x3/r_jet,2.0))/2.0; // Only half the energy downwards
		v_cell = v_jet;
                if (quasar_mode == 1 && AGN_mode < 4) {
                  E_quasar_cell = f_quasar*E_jet_cell; // power going into quasar mode
                  E_jet_cell = (1.0-f_quasar)*E_jet_cell; // remove the fraction of power that goes to quasar mode
                  rho_quasar = 2.0*E_quasar_cell/pow(v_cell,2.0);
                } else if (quasar_mode == 1 && AGN_mode >= 4 && dM/dt >= macc_quasar) {
                  E_quasar_cell = f_quasar*E_jet_cell; // power going into quasar mode
                  E_jet_cell = (1.0-f_quasar)*E_jet_cell; // remove the fraction of power that goes to quasar mode
                  rho_quasar = 2.0*E_quasar_cell/pow(v_cell,2.0);
                } 
		rho_jet = 2.0*E_jet_cell/pow(v_cell,2.0);
		
		// Pure precession
		omega_jet_prec = 2.0*PI/dt_jet_prec;
		v_x_jet = v_cell*sin(theta_jet*fac_ang)*cos(omega_jet_prec*(t_hydro-t_AGN));
		v_y_jet = v_cell*sin(theta_jet*fac_ang)*sin(omega_jet_prec*(t_hydro-t_AGN));
		v_z_jet = v_cell*cos(theta_jet*fac_ang);
		
		// update cons variables
		pGrid->U[k][j][i].d += rho_jet;
		pGrid->U[k][j][i].M1 += -rho_jet*v_x_jet;
		pGrid->U[k][j][i].M2 += -rho_jet*v_y_jet;
		pGrid->U[k][j][i].M3 += -rho_jet*v_z_jet;
		// Gaussian 
		Eth = fac_t*f_th*(1-f_quasar)*dP_jet*dt*exp(-0.5*pow(r/r_jet,2.0))*exp(-0.5*pow(x3/r_jet,2.0))/2.0;
		KE = 0.5*rho_jet*v_cell*v_cell;
		ME = 0.0;
		pGrid->U[k][j][i].E += Eth + ME + KE;
#if (NSCALARS > 3)
		pGrid->U[k][j][i].s[3] += rho_jet; // jet passive tracer
#endif

		// Inject quasar wind
		if (quasar_mode == 1) {
		  // wind angle 
		  quasar_angle = pow(x1*x1+x2*x2,0.5)/r_AGN*theta_quasar;
		  v_x_quasar = v_cell*sin(quasar_angle)*x1/pow(x1*x1+x2*x2,0.5);
		  v_y_quasar = v_cell*sin(quasar_angle)*x2/pow(x1*x1+x2*x2,0.5);
		  v_z_quasar = v_cell*cos(quasar_angle);

		  // update cons variables
		  pGrid->U[k][j][i].d += rho_quasar;
		  pGrid->U[k][j][i].M1 += rho_quasar*v_x_quasar;
		  pGrid->U[k][j][i].M2 += rho_quasar*v_y_quasar;
		  pGrid->U[k][j][i].M3 += -rho_quasar*v_z_quasar;
		  // Gaussian
		  Eth = fac_t*f_th*f_quasar*dP_jet*dt*exp(-0.5*pow(r/r_jet,2.0))*exp(-0.5*pow(x3/r_jet,2.0))/2.0;
		  KE = 0.5*rho_quasar*v_cell*v_cell;
		  ME = 0.0;
		  pGrid->U[k][j][i].E += Eth + ME + KE;
#if (NSCALARS > 3)
		  pGrid->U[k][j][i].s[3] += rho_quasar; // jet passive tracer
#endif
		}

	      }
	    } else if (bubble_mode > 0 && M_up > 0.0 && M_down > 0.0) {
	      // This is for the bubble model

	      // Pure precession
	      omega_jet_prec = 2.0*PI/dt_jet_prec;
	      fac_ang = 1.0;
	      x_b_up = r_bubble*sin(theta_bubble*fac_ang)*cos(phi_bubble);
	      y_b_up = r_bubble*sin(theta_bubble*fac_ang)*sin(phi_bubble);
	      z_b_up = r_bubble*cos(theta_bubble*fac_ang);
	      x_b_down = -r_bubble*sin(theta_bubble*fac_ang)*cos(phi_bubble);
	      y_b_down = -r_bubble*sin(theta_bubble*fac_ang)*sin(phi_bubble);
	      z_b_down = -r_bubble*cos(theta_bubble*fac_ang);
	      
	      r_up = sqrt(SQR(x1-x_b_up) + SQR(x2-y_b_up) + SQR(x3-z_b_up));
              r_down = sqrt(SQR(x1-x_b_down) + SQR(x2-y_b_down) + SQR(x3-z_b_down));

	      if (r_up < r_AGN && pDomain->Level == LevelMax) {
		E_jet_cell = dP_jet*dt/2.0; // only half of the energy goes upwards
		rho_b_up = 5.0*M_up*pow(dx/r_AGN,5.0)/(4.0*PI*pow(dx,3.0));

                // update cons variables
                //pGrid->U[k][j][i].d = MAX(dfloor,rho_b_up*pow(r_up/dx,2.0));
                Eth = E_jet_cell;
                KE = 0.0;
                ME = 0.0;
                pGrid->U[k][j][i].E += Eth + ME + KE;
#if (NSCALARS > 3)
                pGrid->U[k][j][i].s[3] += 3.0*M_up/(4.0*PI*pow(r_AGN,3.0)); // jet passive tracer
#endif
	      }

	      // Downward hot spot
	      if (r_down < r_AGN && pDomain->Level == LevelMax){
		E_jet_cell = dP_jet*dt/2.0; // only half of the energy goes downwards
		rho_b_down = 5.0*M_down*pow(dx/r_AGN,5.0)/(4.0*PI*pow(dx,3.0));

                // update cons variables
                //pGrid->U[k][j][i].d = MAX(dfloor,rho_b_down*pow(r_down/dx,2.0));
                Eth = E_jet_cell;
                KE = 0.0;
                ME = 0.0;
                pGrid->U[k][j][i].E += Eth + ME + KE;
#if (NSCALARS > 3)
                pGrid->U[k][j][i].s[3] += 3.0*M_down/(4.0*PI*pow(r_AGN,3.0)); // jet passive tracer
#endif
              }
	    }
	  }
	}
      }
    }
  }

  return;
}

static void integrate_coolheat(DomainS *pDomain, const Real dt_hydro, const Real dM, const Real t_hydro, const int LevelMax)
{
  GridS *pGrid = pDomain->Grid;
  Real my_tcool, my_dtmin, dtmin;
  Real d, P, ME, KE;
  int ierr, n_sub;
  Real x1,x2,x3,r;
  Real x1tmin,x2tmin, x3tmin, rtmin, rhotmin, Ttmin; 
  int i,j,k;
  int is,ie,js,je,ks,ke;

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;

  /* find the shortest cooling time on the grid */
  my_dtmin = dt_hydro;
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1+x2*x2+x3*x3);
        if (r*r_0/c_nfw/r_s < 2.0){ //&&(r*r_0/c_nfw/r_s > 0.2)){
          d = pGrid->U[k][j][i].d;

          KE = (SQR(pGrid->U[k][j][i].M1) +
                SQR(pGrid->U[k][j][i].M2) +
                SQR(pGrid->U[k][j][i].M3)) / (2.0 * d);

          ME = 0.0;
#ifdef MHD
          ME = (SQR(pGrid->U[k][j][i].B1c) +
                SQR(pGrid->U[k][j][i].B2c) +
                SQR(pGrid->U[k][j][i].B3c)) * 0.5;
#endif  /* MHD */

          P = (pGrid->U[k][j][i].E - ME - KE) * Gamma_1;
          if (P/d > 1.2*Tcool){
            if (heating == 0 || heating == 3){
	      //if (P !=P || d!=d || P/d != P/d || d <= 0.0 || P <= 0.0) {
	      //ath_pout(0, "[integrate_cool]: P, d, P/d  = %e %e %e \n", P, d, P/d);
	      //}
              my_tcool = (cool_only(d, P) > 0) ? (P / Gamma_1) / cool_only(d, P) : my_dtmin; /* use dt=0 in cool() */
	      //ath_pout(0, "[integrate_cool]: U/Edot, my_tcool, my_dtmin  =  %e %e %e \n", (P/Gamma_1)/cool_only(d, P), my_tcool, my_dtmin);	      
            } else if (heating == 1 || heating == 2){ // estimate dt using simple heating since density dependent requires knowing average rho
              my_tcool = (coolheat_simple(d, P, r,dM, dt_hydro) > 0) ? (P / Gamma_1) / coolheat_simple(d, P, r,dM,dt_hydro) : my_dtmin; /* use dt=0 in cool() */
            } 
            if (my_tcool <= 0.){
              my_tcool = 1.e3;
            }
            if (my_tcool*0.25 < my_dtmin) {
              my_dtmin = 0.25 * my_tcool;
              x1tmin = x1; 
              x2tmin = x2;
              x3tmin = x3;
              rtmin = r;
              rhotmin = d;
              Ttmin = P/d;
            } 
            my_dtmin = MIN(my_dtmin, 0.25 * my_tcool); /* CFL = 0.25? */
            if (my_dtmin <= 0.){
              my_dtmin = 1.e3;
            }
          }
        }
      }
    }
  }


  /* sync over all processors */
#ifdef MPI_PARALLEL
  ierr = MPI_Allreduce(&my_dtmin, &dtmin, 1,
                       MPI_DOUBLE, MPI_MIN, pDomain->Comm_Domain);
#else
  dtmin = my_dtmin;
#endif /* MPI_PARALLEL */

  ath_pout(0, "[integrate_cool]: dtmin   =  %e ", dtmin);
  ath_pout(0, " dthydro =  %e ", dt_hydro);

  /* run the subcycles */
  if (dt_hydro / dtmin <= 20) { 
    n_sub = (int) ceil(dt_hydro / dtmin);
    n_sub = MIN(n_sub,20);
  } else {
    n_sub = 20;
  }

  ath_pout(0, "  running %d subcycles.\n", n_sub);

  for (i=0; i<n_sub; i++) {
    cool_step(pDomain, dt_hydro/n_sub, dM/n_sub, t_hydro, i, n_sub, LevelMax);
  }

  return;
}
/* ========================================================================== */

/* ========================================================================== */
static Real enforce_floor(DomainS *pDomain)
{
  /* Enforce T>=Tfloor and d>dfloor everywhere */
  GridS *pGrid = pDomain->Grid;
  PrimS W;
  ConsS U;
  Real T,d,v,T_old,d_old,v1_old,v2_old,v3_old,v_old;
  Real excess_Ekin;
  Real x1,x2,x3, r;
  int i,j,k,ierr;
  int is,ie,js,je,ks,ke;
  int iscalar, jscalar;
  Real mass; // this is a holder for the records
  Real Tvir, Tc;
  Real Tmin, dstar, dmean;
  Real entropy_min, entropy_max, heating_max;

  //Tmin = Tfloor; // Tfloor/(pow(2.0,pDomain->Level)); // Tmin = MAX(Tigm/(pow(2.0,pDomain->Level)), Tfloor);

  Real my_m_added   = 0.0;  Real m_added   = 0.0;
  Real my_m_removed = 0.0;  Real m_removed = 0.0;
  
  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;

  // these control temps are in code units (note that hst are in physical)
  Tvir = kT_keV_inverse(4.88*pow(M200*hubble/1.0e15,2.0/3.0));
  Tc   = kT_keV_inverse(0.0215433310027);

  /* for (k=ks-nghost; k<=ke+nghost; k++) {
    for (j=js-nghost; j<=je+nghost; j++) {
    for (i=is-nghost; i<=ie+nghost; i++) {*/
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1+x2*x2+x3*x3);
        W = Cons_to_Prim(&(pGrid->U[k][j][i]));
        T = W.P / W.d;
        d = W.d;
	v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
        if (r < 2.0*c_nfw*r_s/r_0){

          // Protect the code from NaNs and negative pressure/density
          if (W.P != W.P || W.d!=W.d || W.P/W.d != W.P/W.d || W.d <= 0.0 || W.P <= 0.0) {
            //ath_pout(-1, "[enforce_floor]: Big problem at %f, %f, %f in d, P = %f, %f\n",x1,x2,x3,W.d,W.P);

	    dmean = dfloor; //exp((log(dfloor)+log(dceil))/2.0);
            W.d = dmean;
	    Tmin = Tfloor; //*pow((dmean*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
            W.P = Tmin * W.d;

            W.V1 = 0.0;
            W.V2 = 0.0;
            W.V3 = 0.0;
            U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].M1 = U.M1;
            pGrid->U[k][j][i].M2 = U.M2;
            pGrid->U[k][j][i].M3 = U.M3; 
	  }
	  
	  // Density Ceiling
          if (d > dceil){
            //ath_pout(-1, "[enforce_floor]: hit density ceiling at %f, %f, %f, density was %f\n", x1,x2,x3,d);
            mass = (d - dceil)* pGrid->dx1 * pGrid->dx2 * pGrid->dx3;
            my_m_removed += mass;
	    // Enforce density ceiling, keep temperature fixed, allowing pressure to decrease.
	    //W.d = dceil;
	    //Tmin = Tfloor*pow((dceil*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
            //W.P = MAX(T,Tmin) * W.d; // do not allow T<Tmin 

            // Test: enforce density ceiling, but keep pressure/energy fixed
	    W.V1 = pow(W.d/dceil,0.5)*W.V1;
            W.V2 = pow(W.d/dceil,0.5)*W.V2;
            W.V3 = pow(W.d/dceil,0.5)*W.V3;
            Tmin = Tfloor; //*pow((dceil*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
            W.P = MAX(W.P,Tmin*dceil);
            W.d = dceil; 

	    U = Prim_to_Cons(&W); 
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].M1 = U.M1;
            pGrid->U[k][j][i].M2 = U.M2;
            pGrid->U[k][j][i].M3 = U.M3;          
          } 

	  // Density Floor
	  if (d < dfloor){
            //ath_pout(-1, "[enforce_floor]: hit density floor at %f, %f, %f, density was %f\n", x1,x2,x3,d);
            mass = (dfloor - d)* pGrid->dx1 * pGrid->dx2 * pGrid->dx3;
            my_m_added += mass;
	    // Enforce density floor, keep temperature fixed, allowing pressure to increase.
            /* W.d = dfloor;
	    Tmin = Tfloor*pow((dfloor*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
            W.P = MAX(T,Tmin) * W.d;

            U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].M1 = U.M1;
            pGrid->U[k][j][i].M2 = U.M2;
            pGrid->U[k][j][i].M3 = U.M3; */

	    // Test: enforce density floor, but keep pressure/energy fixed
	    W.V1 = pow(W.d/dfloor,0.5)*W.V1;
	    W.V2 = pow(W.d/dfloor,0.5)*W.V2;
	    W.V3 = pow(W.d/dfloor,0.5)*W.V3;
            Tmin = Tfloor; //*pow((dfloor*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
	    W.P = MAX(W.P,Tmin*dfloor);
	    W.d = dfloor;
	    
            U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].M1 = U.M1;
            pGrid->U[k][j][i].M2 = U.M2;
            pGrid->U[k][j][i].M3 = U.M3;
	    } 

	  // Temperature Ceiling
	  W = Cons_to_Prim(&(pGrid->U[k][j][i]));
	  T = W.P / W.d;
	  d = W.d;
	  v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
	  if (T > Tceil){
            //ath_pout(-1, "[enforce_floor]: hit temperature ceiling at %f, %f, %f, temperature was %f\n", x1,x2,x3,T);
	    T=Tceil;
	    W.P = T * W.d;
	    U = Prim_to_Cons(&W);
	    pGrid->U[k][j][i].E  = U.E;
	  }

          /* Temperature Floor */
          W = Cons_to_Prim(&(pGrid->U[k][j][i]));
          T = W.P / W.d;
          d = W.d;
          v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
          Tmin = Tfloor; //*pow((d*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
          if (T < Tmin){
	    //ath_pout(-1, "[enforce_floor]: hit temperature floor at %f, %f, %f, temperature was %f\n", x1,x2,x3,T);
            W.P = Tmin * W.d;
            U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
          }

	  // Velicity Ceiling
	  W = Cons_to_Prim(&(pGrid->U[k][j][i]));
	  T = W.P / W.d;
	  d = W.d;
	  v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
	  if (fabs(W.V1) > vceil ){
            //ath_pout(-1, "[enforce_floor]: hit V1 ceiling at %f, %f, %f, V1 was %f\n", x1,x2,x3,W.V1);
            W.d = W.d;
	    excess_Ekin = 0.5*W.d*(SQR(W.V1)-SQR(vceil)) ;
            W.P = T * W.d; // + excess_Ekin*Gamma_1;	    
	    W.V1 = W.V1/fabs(W.V1)*vceil;
            U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].M1 = U.M1;
            pGrid->U[k][j][i].M2 = U.M2;
            pGrid->U[k][j][i].M3 = U.M3;
          }
	  W = Cons_to_Prim(&(pGrid->U[k][j][i]));
	  T = W.P / W.d;
	  d = W.d;
	  v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
	  if (fabs(W.V2) > vceil ){
            //ath_pout(-1, "[enforce_floor]: hit V2 ceiling at %f, %f, %f, V2 was %f\n", x1,x2,x3,W.V2);
            W.d = W.d;
            excess_Ekin = 0.5*W.d*(SQR(W.V2)-SQR(vceil)) ;
            W.P = T * W.d; // + excess_Ekin*Gamma_1;
            W.V2 = W.V2/fabs(W.V2)*vceil;
	    U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].M1 = U.M1;
            pGrid->U[k][j][i].M2 = U.M2;
            pGrid->U[k][j][i].M3 = U.M3;
          }
	  W = Cons_to_Prim(&(pGrid->U[k][j][i]));
	  T = W.P / W.d;
	  d = W.d;
	  v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
          if (fabs(W.V3) > vceil ){
            //ath_pout(-1, "[enforce_floor]: hit V3 ceiling at %f, %f, %f, V3 was %f\n", x1,x2,x3,W.V3);
            W.d = W.d;
            excess_Ekin = 0.5*W.d*(SQR(W.V3)-SQR(vceil)) ;
            W.P = T * W.d; // + excess_Ekin*Gamma_1;
            W.V3 = W.V3/fabs(W.V3)*vceil;
	    U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].M1 = U.M1;
            pGrid->U[k][j][i].M2 = U.M2;
            pGrid->U[k][j][i].M3 = U.M3;
          }

	  // Magnetic field ceiling
#ifdef MHD
          W = Cons_to_Prim(&(pGrid->U[k][j][i]));
          T = W.P / W.d;
          d = W.d;
          v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
          if (fabs(W.B1c)/pow(d,0.5) > vceil ){
	    W.d = W.d;
	    excess_Ekin = 0.5*(SQR(W.B1c)-SQR(W.B1c/fabs(W.B1c)*vceil*pow(d,0.5)));
            W.P = T * W.d; // + excess_Ekin*Gamma_1;
	    W.B1c = W.B1c/fabs(W.B1c)*vceil*pow(d,0.5);
	    U = Prim_to_Cons(&W);
	    pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].B1c = U.B1c;
            pGrid->U[k][j][i].B2c = U.B2c;
            pGrid->U[k][j][i].B3c = U.B3c;
	  }
          W = Cons_to_Prim(&(pGrid->U[k][j][i]));
          T = W.P / W.d;
          d = W.d;
          v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
          if (fabs(W.B2c)/pow(d,0.5) > vceil ){
            W.d = W.d;
	    excess_Ekin = 0.5*(SQR(W.B2c)-SQR(W.B2c/fabs(W.B2c)*vceil*pow(d,0.5)));
	    W.P = T * W.d; // + excess_Ekin*Gamma_1;
            W.B2c = W.B2c/fabs(W.B2c)*vceil*pow(d,0.5);
            U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].B1c = U.B1c;
            pGrid->U[k][j][i].B2c = U.B2c;
            pGrid->U[k][j][i].B3c = U.B3c;
          }
          W = Cons_to_Prim(&(pGrid->U[k][j][i]));
          T = W.P / W.d;
          d = W.d;
          v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
          if (fabs(W.B3c)/pow(d,0.5) > vceil ){
            W.d = W.d;
	    excess_Ekin = 0.5*(SQR(W.B3c)-SQR(W.B3c/fabs(W.B3c)*vceil*pow(d,0.5)));
	    W.P = T * W.d; // + excess_Ekin*Gamma_1;
            W.B3c = W.B3c/fabs(W.B3c)*vceil*pow(d,0.5);
            U = Prim_to_Cons(&W);
            pGrid->U[k][j][i].E  = U.E;
            pGrid->U[k][j][i].d  = U.d;
            pGrid->U[k][j][i].B1c = U.B1c;
            pGrid->U[k][j][i].B2c = U.B2c;
            pGrid->U[k][j][i].B3c = U.B3c;
          }
#endif /* MHD */

	  // Passive scalars
	  W = Cons_to_Prim(&(pGrid->U[k][j][i]));
          T = W.P / W.d;
          d = W.d;
	  Tmin = Tfloor; //*pow((dfloor*rho_0/mu/1.66e-24/0.1),Gamma_1);
	  entropy_min = Tmin*dfloor/pow(dceil,Gamma_1);
	  entropy_max = Tceil*dceil/pow(dfloor,Gamma_1);
	  heating_max = pow(dceil,Gamma_1)/Gamma_1*(entropy_max-entropy_min);
#if (NSCALARS > 0)
          for (iscalar=0; iscalar<=NSCALARS; iscalar++) {
	    // Protect from NaN or infinity values
            if (pGrid->U[k][j][i].s[iscalar] != pGrid->U[k][j][i].s[iscalar] || isinf(fabs(pGrid->U[k][j][i].s[iscalar]))) {
#ifdef EXPLICIT_HEATING
              //ath_pout(-1, "[enforce_floor]: Problem with scalar %d at %f, s = %f\n", iscalar,r,pGrid->U[k][j][i].s[iscalar]);
	      for (jscalar=0; jscalar<=NSCALARS; jscalar++) {
		if (jscalar <= 1) {
		  dmean = dfloor; //exp((log(dfloor)+log(dceil))/2.0);
		  Tmin = Tfloor; //*pow((dmean*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
		  pGrid->U[k][j][i].s[0] = Tmin*dmean/pow(dmean,Gamma_1);//entropy_min; // Conserved entropy field
		  pGrid->U[k][j][i].s[1] = 0.0;
		} 
		if (jscalar >= 2) {
		  pGrid->U[k][j][i].s[jscalar] = 0.0;
		}
	      }
#else
              pGrid->U[k][j][i].s[iscalar] = 0.0;
#endif
	      /* dmean = dfloor; //exp((log(dfloor)+log(dceil))/2.0);
	      W.d = dmean;
	      Tmin = Tfloor*pow((dmean*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin 
	      W.P = Tmin * dmean;
	      W.V1 = 0.0;
	      W.V2 = 0.0;
	      W.V3 = 0.0;
	      U = Prim_to_Cons(&W);
	      pGrid->U[k][j][i].E  = U.E;
	      pGrid->U[k][j][i].d  = U.d;
	      pGrid->U[k][j][i].M1 = U.M1;
	      pGrid->U[k][j][i].M2 = U.M2;
	      pGrid->U[k][j][i].M3 = U.M3; */
            }

#ifdef EXPLICIT_HEATING	    
	    // Enforce entropy ceiling and heating ceiling
	    if (iscalar <= 1 && ( fabs(pGrid->U[k][j][i].s[0]) > entropy_max || fabs(pGrid->U[k][j][i].s[1]) > heating_max ) ) {
	      dmean = dfloor; //exp((log(dfloor)+log(dceil))/2.0);
              Tmin = Tfloor; //*pow((dmean*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin 
	      pGrid->U[k][j][i].s[0] = Tmin*dmean/pow(dmean,Gamma_1); //entropy_max; 
	      pGrid->U[k][j][i].s[1] = heating_max; 
              /* W.d = dmean;
	      W.P = Tmin * dmean;
              W.V1 = 0.0;
              W.V2 = 0.0;
              W.V3 = 0.0;
              U = Prim_to_Cons(&W);
              pGrid->U[k][j][i].E  = U.E;
              pGrid->U[k][j][i].d  = U.d;
              pGrid->U[k][j][i].M1 = U.M1;
              pGrid->U[k][j][i].M2 = U.M2;
              pGrid->U[k][j][i].M3 = U.M3; */
	    }

	    // Enforce entropy floor 
            if (iscalar == 0  && pGrid->U[k][j][i].s[0] < entropy_min ) {
	      dmean = dfloor;
              Tmin = Tfloor; //*pow((dmean*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
              pGrid->U[k][j][i].s[0] = Tmin*dmean/pow(dmean,Gamma_1); //entropy_min;
              pGrid->U[k][j][i].s[1] = 0.0;
              /* W.d = dmean; //dceil;
              W.P = Tmin * dmean; //Tmin * dfloor;
              W.V1 = 0.0;
              W.V2 = 0.0;
              W.V3 = 0.0;
              U = Prim_to_Cons(&W);
              pGrid->U[k][j][i].E  = U.E;
              pGrid->U[k][j][i].d  = U.d;
              pGrid->U[k][j][i].M1 = U.M1;
              pGrid->U[k][j][i].M2 = U.M2;
              pGrid->U[k][j][i].M3 = U.M3; */
            }
	    
	    // Enforce floor for other passive scalar
	    if (iscalar >= 3 && pGrid->U[k][j][i].s[iscalar] <= 0.0) {
	      pGrid->U[k][j][i].s[iscalar] = 0.0;
	    } 

	    if (iscalar >= 3 && pGrid->U[k][j][i].s[iscalar]/pGrid->U[k][j][i].d > 100.0) {
	      pGrid->U[k][j][i].s[iscalar] = 100.0*pGrid->U[k][j][i].d;
            }

#endif
          }
#endif /* PASSIVELY ADVECTED SCALARS*/

	}
      }
    }
  }    

#ifdef MPI_PARALLEL 
  ierr = MPI_Allreduce(&my_m_added, &m_added, 1,
                       MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
  ierr = MPI_Allreduce(&my_m_removed, &m_removed, 1,
                       MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
#else
  m_added = my_m_added;
  m_removed = my_m_removed;
#endif /* MPI_PARALLEL */

  //ath_pout(0, "[enforce_floor]: t = %f \t m_removed = %e \t m_added = %e \n",pGrid->time, m_removed, m_added);

  if (m_added <= m_removed){
    return m_removed - m_added;
  } else {
    return 0.0;
  }
}
/* ========================================================================== */
/* ========================================================================== */

static Real star_formation(DomainS *pDomain)
{
  /* Enforce T>=Tfloor and d>dfloor everywhere */
  GridS *pGrid = pDomain->Grid;
  PrimS W;
  ConsS U;
  Real T,d,v,T_old,d_old,v1_old,v2_old,v3_old,v_old;
  Real excess_Ekin;
  Real x1,x2,x3, r;
  int i,j,k,ierr;
  int is,ie,js,je,ks,ke;
  int iscalar;
  Real mass; // this is a holder for the records
  Real Tvir, Tc;
  Real Tmin, dstar;

  //Tmin = Tfloor; // Tfloor/(pow(2.0,pDomain->Level)); // Tmin = MAX(Tigm/(pow(2.0,pDomain->Level)), Tfloor);

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;

  // these control temps are in code units (note that hst are in physical)
  Tvir = kT_keV_inverse(4.88*pow(M200*hubble/1.0e15,2.0/3.0));
  Tc   = kT_keV_inverse(0.0215433310027);

  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1+x2*x2+x3*x3);
        if (r < 2.0*c_nfw*r_s/r_0){
          /* STAR FORMATION TRICK */
          W = Cons_to_Prim(&(pGrid->U[k][j][i]));
          T = W.P / W.d;
          d = W.d;
          v = sqrt(pow(W.V1,2.0)+pow(W.V2,2.0)+pow(W.V3,2.0));
          Tmin = Tfloor*pow((d*rho_0/mu/1.66e-24/0.1),Gamma_1); // density-dependent Tmin
          if (T < 2.0*Tfloor && d*rho_0 > mu*1.66e-24*0.1){
            dstar = MIN(0.9*d, pow(4.0*PI*6.67e-8,0.5)*pow(d,1.5)*pGrid->dt/100.);
            W.d = d - dstar;
            W.P = MAX(T,Tmin) * W.d;
            U = Prim_to_Cons(&W);
	    pGrid->U[k][j][i].d = U.d;
            pGrid->U[k][j][i].E = U.E;
          }
	}
      }
    }
  }    
  return 0.0;
}
/* ========================================================================== */
/* ========================================================================== */

static Real InwardMassFlux(DomainS *pDomain, const Real racc)
{
  GridS *pGrid = pDomain->Grid;
  PrimS W;
  Real v;
  Real x1,x2,x3, r;
  int i,j,k,ierr;
  int is,ie,js,je,ks,ke;
  Real my_flux, total_flux;
  int my_ncells, total_ncells;

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;

  my_flux = 0.0;
  my_ncells = 0;
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        W = Cons_to_Prim(&(pGrid->U[k][j][i]));
        r = sqrt(x1*x1 + x2*x2 + x3*x3);
        v = (W.V1*x1 + W.V2*x2 + W.V3*x3)/r;
	if (AGN_mode <= 4) {
	  my_flux   += ((r <= racc)
			&&(r > racc-(1.000*pGrid->dx1))
			&&(v <= 0.0)
			&&(W.P/W.d < Tacc)) ? W.d * v * 4.0*PI*SQR(racc) : 0.0; 
	  my_ncells += ((r <= racc)
			&&(r > racc-(1.000*pGrid->dx1))
			&&(W.P/W.d < Tacc)) ? 1 : 0;                   
	} else if (AGN_mode == 5) {
	  my_flux   += ((r <= racc)
                        &&(W.P/W.d < Tacc)) ? W.d * pow(pGrid->dx1,3.0) / t_ff : 0.0;
	  my_ncells += ((r <= racc)
			&&(W.P/W.d < Tacc)) ? 1 : 0;
	}
      }
    }
  }

#ifdef MPI_PARALLEL 
  ierr = MPI_Allreduce(&my_flux, &total_flux, 1,
                       MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
  ierr = MPI_Allreduce(&my_ncells, &total_ncells, 1,
                       MPI_INT, MPI_SUM, pDomain->Comm_Domain);  
#else
  total_flux = my_flux;
  total_ncells = my_ncells;
#endif

  if (AGN_mode == 5) total_ncells = 1;

  return fabs(total_flux)/total_ncells;
}

/* ========================================================================== */
/* ========================================================================== */

static Real Central_Cooling(DomainS *pDomain)
{
  GridS *pGrid = pDomain->Grid;
  int i, j, k, ierr;
  Real KE, ME, d, P, T;
  int is,ie,js,je,ks,ke;
  Real x1,x2,x3,r,dx1,dx2,dx3;
  Real lam1,lam2,lam3,lam4;
  Real my_Edotcool_central, total;

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;

  dx1 = pGrid->dx1;
  dx2 = pGrid->dx2;
  dx3 = pGrid->dx3;

  my_Edotcool_central = 0.0;  
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1+x2*x2+x3*x3);
	if (r < r_power) {
	  d = pGrid->U[k][j][i].d;
	  KE = (SQR(pGrid->U[k][j][i].M1) +
		SQR(pGrid->U[k][j][i].M2) +
		SQR(pGrid->U[k][j][i].M3)) / (2.0 * d);
	  ME = 0.0;
#ifdef MHD
	  ME = (SQR(pGrid->U[k][j][i].B1c) +
		SQR(pGrid->U[k][j][i].B2c) +
		SQR(pGrid->U[k][j][i].B3c)) * 0.5;
#endif  /* MHD */
	  P = (pGrid->U[k][j][i].E - ME - KE) * Gamma_1;
	  lam1 = cool_only(d,P);
	  lam2 = cool_only(d,P-(Gamma_1*(lam1/2.)));
	  lam3 = cool_only(d,P-(Gamma_1*(lam2/2.)));
	  lam4 = cool_only(d,P-(Gamma_1*lam3));
	  my_Edotcool_central += dx1*dx2*dx3*(lam1 + 2.*lam2 + 2.*lam3 + lam4)/6.;
	}
      }
    }
  }
  
#ifdef MPI_PARALLEL
  MPI_Barrier( pDomain->Comm_Domain );
  ierr = MPI_Allreduce(&my_Edotcool_central, &total, 1, MPI_DOUBLE, MPI_SUM, pDomain->Comm_Domain);
#else
  total = my_Edotcool_central;
#endif

  return total;
}

/* ========================================================================== */
/* ========================================================================== */
/* =====================     End Of Cooling    ============================== */
/* ========================================================================== */
/* ========================================================================== */


/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  set_vars();

  return;
}

ConsFun_t get_usr_expr(const char *expr)
{
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name){
  return NULL;
}

void Userwork_in_loop(MeshS *pM)
{
  int i,j,k,nl,nd,ierr;
  int nx1,nx2,nx3; 
  int is,ie,il,iu,js,je,jl,ju,ks,ke,kl,ku;
  Real x1,x2,x3,r;
  Real T, Pres, dM;
  Real Delta_M_sum=0., Delta_t_sum=0.;
  Real minsize, temp, my_Edotcool_central, mdot_acc, my_mdot_acc;

  Edotcool_central = 0.0;
  my_Edotcool_central = 0.0;
  my_mdot_acc = 0.0;
  //if (AGN_mode == 3 || AGN_mode == 4 || heating >0) {
    for (nl=0; nl<=(pM->NLevels)-1; nl++) {
      for (nd=0; nd<=(pM->DomainsPerLevel[nl])-1; nd++) {
	if (AGN_mode ==3) {
	  minsize = MIN(pM->Domain[nl][nd].MaxX[0]-pM->Domain[nl][nd].MinX[0],
			pM->Domain[nl][nd].MaxX[1]-pM->Domain[nl][nd].MinX[1]);
	  minsize = MIN(pM->Domain[nl][nd].MaxX[2]-pM->Domain[nl][nd].MinX[2],
			minsize);
	  if (minsize >= 2*r_power) {
	    if (pM->Domain[nl][nd].Grid != NULL) {
	      temp = Central_Cooling(&(pM->Domain[nl][nd]));
	    } else {
	      temp = 0.0;
	    }
	    my_Edotcool_central = MAX(my_Edotcool_central, temp);
	  }
	}

	if (pM->Domain[nl][nd].Grid != NULL) {
	  temp = InwardMassFlux(&(pM->Domain[nl][nd]), r_AGN);
	  if (temp != temp) temp = 0.0;
	  my_mdot_acc = temp;
	} 
      }
    }
    //}
  ierr = MPI_Allreduce(&my_Edotcool_central, &Edotcool_central, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_mdot_acc, &mdot_acc, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

  if (bubble_mode > 0) {
    phi_bubble=2.0*PI*ran2(&iseed);
    theta_bubble=PI*ran2(&iseed)/2.0;
    r_bubble=bubble_height*ran2(&iseed);
    MPI_Bcast(&phi_bubble, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&theta_bubble, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&r_bubble, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    //ath_pout(-1,"%e %e %e \n",r_bubble,phi_bubble,theta_bubble);
  }

  for (nl=0; nl<=(pM->NLevels)-1; nl++) {
    for (nd=0; nd<=(pM->DomainsPerLevel[nl])-1; nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) {
        // inner_bc(&(pM->Domain[nl][nd])); // used by Drummond for cosmological infall
	/* Cooling, heating and feedback */
        if ((cooling > 0)||(heating > 0)){
          enforce_floor(&(pM->Domain[nl][nd])); 
	  dM = mdot_acc*pM->dt;
	  integrate_coolheat(&(pM->Domain[nl][nd]), pM->dt, dM, pM->time, (pM->NLevels)-1);
          //star_formation(&(pM->Domain[nl][nd]));
	  enforce_floor(&(pM->Domain[nl][nd]));
        }
      }
    }
  }
  if(pM->dt < 1e-12){
    for (nl=0; nl<=(pM->NLevels)-1; nl++) {
      for (nd=0; nd<=(pM->DomainsPerLevel[nl])-1; nd++) {
        if (pM->Domain[nl][nd].Grid != NULL) {
          FindProblemCell(&(pM->Domain[nl][nd]));
        }
      }
    }
    MPI_Barrier( MPI_COMM_WORLD ) ; 
    ath_error("[Userwork_in_loop]: dt has gotten too small %e\n", pM->dt);
  }
  
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}
/* end user work */
/* ========================================================================== */


/* ========================================================================== */
/* gravity */

/* NFW potential. */
static Real phi_nfw(const Real x)
{
  Real b, fact, phi_unit;
  Real fact_BCG, r4, internal, external;

  b = r_0/r_s;
  fact = rho_s/rho_0*pow(r_s/r_0,2.0)*log(1.0+b*x)/(b*x);

  if (BCG_potential == 1) {
    r4 = 4.0*3.08e21/r_0;
    phi_unit = 4.0*PI*6.67e-8*rho_0*pow(r_0,2.0);
    internal = (6.67e-8*M_BCG_4*2e33) / (r_0*x) / phi_unit * pow(2.0,-1.43) * pow(x/r4,0.1) * pow(1.0+x/r4,1.33);
    external = +(6.67e-8*M_BCG_4*2e33/4.0/3.08e21) / phi_unit * pow(2.0,-1.43) * \
      ( -0.11111*pow(x/r4,-0.9) -0.14788*pow(x/r4,-0.33533) \
	+1.47777*pow(x/r4,0.1) -5.20133*pow(x/r4,0.18389) \
	+3.32558*pow(x/r4,0.43) ); // fitted integrals with power laws, accurate at per-cent level
    fact_BCG = internal + external; 
    //ath_pout(-1,"r, NFW_potential, BCG_potential = %e, %e, %e \n", x, fact, fact_BCG);
    fact = fact + fact_BCG;    
  }

  return fact;
}

static Real grav_pot(const Real x1, const Real x2, const Real x3)
{
  Real phi_tot, phi_in, phi_out;
  Real x, x_switch, width;
  int ind, f_switch;

  x = sqrt(x1*x1+x2*x2+x3*x3);
  ind = ind_switch;
  x_switch = r_switch;

  phi_in  = phi_nfw(x);
  phi_out = phi_nfw(x_switch);  /* phi = const at large radii */

  f_switch = exp(-x/0.9/x_switch);

  /* interpolate between phi_in and phi_out */
  phi_tot = f_switch*pow(fabs(phi_in), ind) + (1-f_switch)*pow(fabs(phi_out), ind);
  phi_tot = -1.0 * pow(phi_tot, 1.0/ind);

  return phi_tot;
}
/* end gravity */
/* ========================================================================== */

/* ========================================================================== */
static Real FindProblemCell(DomainS *pDomain)
{
  GridS *pGrid = pDomain->Grid;
  PrimS W;
  ConsS U;
  Real T, rho, pressure, velocity, energy ;
  Real x1,x2,x3, r;
  int i,j,k,ierr;
  int is,ie,js,je,ks,ke;

  Real my_T_min, T_min;
  Real my_T_min_x,my_T_min_y,my_T_min_z,my_T_min_r,  T_min_x,T_min_y,T_min_z,T_min_r ;
  Real my_T_max, T_max;
  Real my_T_max_x,my_T_max_y,my_T_max_z,my_T_max_r,  T_max_x,T_max_y,T_max_z,T_max_r ;

  Real my_rho_min, rho_min;
  Real my_rho_min_x,my_rho_min_y,my_rho_min_z,my_rho_min_r,  rho_min_x,rho_min_y,rho_min_z,rho_min_r ;
  Real my_rho_max, rho_max;
  Real my_rho_max_x,my_rho_max_y,my_rho_max_z,my_rho_max_r,  rho_max_x,rho_max_y,rho_max_z,rho_max_r ;

  Real my_pressure_min, pressure_min;
  Real my_pressure_min_x,my_pressure_min_y,my_pressure_min_z,my_pressure_min_r,  pressure_min_x,pressure_min_y,pressure_min_z,pressure_min_r ;
  Real my_pressure_max, pressure_max;
  Real my_pressure_max_x,my_pressure_max_y,my_pressure_max_z,my_pressure_max_r,  pressure_max_x,pressure_max_y,pressure_max_z,pressure_max_r ;

  Real my_velocity_min, velocity_min;
  Real my_velocity_min_x,my_velocity_min_y,my_velocity_min_z,my_velocity_min_r,  velocity_min_x,velocity_min_y,velocity_min_z,velocity_min_r ;
  Real my_velocity_max, velocity_max;
  Real my_velocity_max_x,my_velocity_max_y,my_velocity_max_z,my_velocity_max_r,  velocity_max_x,velocity_max_y,velocity_max_z,velocity_max_r ;
  
  Real my_energy_min, energy_min;
  Real my_energy_min_x,my_energy_min_y,my_energy_min_z,my_energy_min_r,  energy_min_x,energy_min_y,energy_min_z,energy_min_r ;
  Real my_energy_max, energy_max;
  Real my_energy_max_x,my_energy_max_y,my_energy_max_z,my_energy_max_r,  energy_max_x,energy_max_y,energy_max_z,energy_max_r ;
  
  my_T_min        =1.e15; my_T_max        =-1.e15;  
  my_rho_min      =1.e15; my_rho_max      =-1.e15;  
  my_pressure_min =1.e15; my_pressure_max =-1.e15;  
  my_velocity_min =1.e15; my_velocity_max =-1.e15;  
  my_energy_min   =1.e15; my_energy_max   =-1.e15;  

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;

  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1+x2*x2+x3*x3);
        if (r <1.5*c_nfw*r_s/r_0){
          W = Cons_to_Prim(&(pGrid->U[k][j][i]));
          T = W.P / W.d;
          rho = W.d;
          velocity = sqrt(W.V1*W.V1 + W.V2*W.V2 + W.V3*W.V3);
          pressure = W.P;
          energy = pGrid->U[k][j][i].E;
          // Temperature 
          if (T < my_T_min){
            my_T_min   = T  ;
            my_T_min_x = x1 ;
            my_T_min_y = x2 ;
            my_T_min_z = x3 ;
            my_T_min_r = r  ;
          }
          if (T > my_T_max){
            my_T_max   = T  ;
            my_T_max_x = x1 ;
            my_T_max_y = x2 ;
            my_T_max_z = x3 ;
            my_T_max_r = r  ;
          }
          // Density
          if (rho < my_rho_min){
            my_rho_min   = rho  ;
            my_rho_min_x = x1 ;
            my_rho_min_y = x2 ;
            my_rho_min_z = x3 ;
            my_rho_min_r = r  ;
          }
          if (rho > my_rho_max){
            my_rho_max   = rho  ;
            my_rho_max_x = x1 ;
            my_rho_max_y = x2 ;
            my_rho_max_z = x3 ;
            my_rho_max_r = r  ;
          }
          // Pressure
          if (pressure < my_pressure_min){
            my_pressure_min   = pressure  ;
            my_pressure_min_x = x1 ;
            my_pressure_min_y = x2 ;
            my_pressure_min_z = x3 ;
            my_pressure_min_r = r  ;
          }
          if (pressure > my_pressure_max){
            my_pressure_max   = pressure  ;
            my_pressure_max_x = x1 ;
            my_pressure_max_y = x2 ;
            my_pressure_max_z = x3 ;
            my_pressure_max_r = r  ;
          }
          // Velocity
          if (velocity < my_velocity_min){
            my_velocity_min   = velocity  ;
            my_velocity_min_x = x1 ;
            my_velocity_min_y = x2 ;
            my_velocity_min_z = x3 ;
            my_velocity_min_r = r  ;
          }
          if (velocity > my_velocity_max){
            my_velocity_max   = velocity  ;
            my_velocity_max_x = x1 ;
            my_velocity_max_y = x2 ;
            my_velocity_max_z = x3 ;
            my_velocity_max_r = r  ;
          }
          // energy
          if (energy < my_energy_min){
            my_energy_min   = energy  ;
            my_energy_min_x = x1 ;
            my_energy_min_y = x2 ;
            my_energy_min_z = x3 ;
            my_energy_min_r = r  ;
          }
          if (energy > my_energy_max){
            my_energy_max   = energy  ;
            my_energy_max_x = x1 ;
            my_energy_max_y = x2 ;
            my_energy_max_z = x3 ;
            my_energy_max_r = r  ;
          }
        }
      }
    }
  }

#ifdef MPI_PARALLEL 
  ierr = MPI_Allreduce(&my_T_min, &T_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_T_max, &T_max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_rho_min, &rho_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_rho_max, &rho_max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_pressure_min, &pressure_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_pressure_max, &pressure_max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_velocity_min, &velocity_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_velocity_max, &velocity_max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_energy_min, &energy_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_energy_max, &energy_max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
#else
  my_T_min        = T_min;
  my_T_max        = T_max;
  my_rho_min      = rho_min;
  my_rho_max      = rho_max;
  my_pressure_min = pressure_min;
  my_pressure_max = pressure_max;
  my_velocity_min = velocity_min;
  my_velocity_max = velocity_max;
  my_energy_min   = energy_min;
  my_energy_max   = energy_max;
#endif /* MPI_PARALLEL */

  if (my_T_min == T_min){
    ath_pout(-1,"[FindProblemCell] T_min = %e          \t x = %f y = %f z = %f r/rvir = %f\n",
	     T_min, my_T_min_x, my_T_min_y, my_T_min_z, my_T_min_r*r_0/c_nfw/r_s );
  }
  if (my_T_max == T_max){
    ath_pout(-1,"[FindProblemCell] T_max = %e          \t x = %f y = %f z = %f r/rvir = %f\n",
	     T_max, my_T_max_x, my_T_max_y, my_T_max_z, my_T_max_r*r_0/c_nfw/r_s );
  }
  if (my_rho_min == rho_min){
    ath_pout(-1,"[FindProblemCell] rho_min = %e        \t x = %f y = %f z = %f r/rvir = %f\n",
	     rho_min, my_rho_min_x, my_rho_min_y, my_rho_min_z, my_rho_min_r*r_0/c_nfw/r_s );
  }
  if (my_rho_max == rho_max){
    ath_pout(-1,"[FindProblemCell] rho_max = %e        \t x = %f y = %f z = %f r/rvir = %f\n",
	     rho_max, my_rho_max_x, my_rho_max_y, my_rho_max_z, my_rho_max_r*r_0/c_nfw/r_s );
  }
  if (my_pressure_min == pressure_min){
    ath_pout(-1,"[FindProblemCell] pressure_min = %e   \t x = %f y = %f z = %f r/rvir = %f\n",
	     pressure_min, my_pressure_min_x, my_pressure_min_y, my_pressure_min_z, my_pressure_min_r*r_0/c_nfw/r_s );
  }
  if (my_pressure_max == pressure_max){
    ath_pout(-1,"[FindProblemCell] pressure_max = %e   \t x = %f y = %f z = %f r/rvir = %f\n",
	     pressure_max, my_pressure_max_x, my_pressure_max_y, my_pressure_max_z, my_pressure_max_r*r_0/c_nfw/r_s );
  }
  if (my_velocity_min == velocity_min){
    ath_pout(-1,"[FindProblemCell] velocity_min = %e   \t x = %f y = %f z = %f r/rvir = %f\n",
	     velocity_min, my_velocity_min_x, my_velocity_min_y, my_velocity_min_z, my_velocity_min_r*r_0/c_nfw/r_s );
  }
  if (my_velocity_max == velocity_max){
    ath_pout(-1,"[FindProblemCell] velocity_max = %e   \t x = %f y = %f z = %f r/rvir = %f\n",
	     velocity_max, my_velocity_max_x, my_velocity_max_y, my_velocity_max_z, my_velocity_max_r*r_0/c_nfw/r_s );
  }
  if (my_energy_min == energy_min){
    ath_pout(-1,"[FindProblemCell] energy_min = %e     \t x = %f y = %f z = %f r/rvir = %f\n",
	     energy_min, my_energy_min_x, my_energy_min_y, my_energy_min_z, my_energy_min_r*r_0/c_nfw/r_s );
  }
  if (my_energy_max == energy_max){
    ath_pout(-1,"[FindProblemCell] energy_max = %e     \t x = %f y = %f z = %f r/rvir = %f\n",
	     energy_max, my_energy_max_x, my_energy_max_y, my_energy_max_z, my_energy_max_r*r_0/c_nfw/r_s );
  }
  return 0.0;
}
/* ========================================================================== */


/* ========================================================================== */

#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define RNMX (1.0-DBL_EPSILON)

/*! \fn double ran2(long int *idum)
 *  \brief Extracted from the Numerical Recipes in C (version 2) code.  Modified
 *   to use doubles instead of floats. -- T. A. Gardiner -- Aug. 12, 2003
 *
 * Long period (> 2 x 10^{18}) random number generator of L'Ecuyer
 * with Bays-Durham shuffle and added safeguards.  Returns a uniform
 * random deviate between 0.0 and 1.0 (exclusive of the endpoint
 * values).  Call with idum = a negative integer to initialize;
 * thereafter, do not alter idum between successive deviates in a
 * sequence.  RNMX should appriximate the largest floating point value
 * that is less than 1.
 */

double ran2(long int *idum)
{
  int j;
  long int k;
  static long int idum2=123456789;
  static long int iy=0;
  static long int iv[NTAB];
  double temp;

  if (*idum <= 0) { /* Initialize */
    if (-(*idum) < 1) *idum=1; /* Be sure to prevent idum = 0 */
    else *idum = -(*idum);
    idum2=(*idum);
    for (j=NTAB+7;j>=0;j--) { /* Load the shuffle table (after 8 warm-ups) */
      k=(*idum)/IQ1;
      *idum=IA1*(*idum-k*IQ1)-k*IR1;
      if (*idum < 0) *idum += IM1;
      if (j < NTAB) iv[j] = *idum;
    }
    iy=iv[0];
  }
  k=(*idum)/IQ1;                 /* Start here when not initializing */
  *idum=IA1*(*idum-k*IQ1)-k*IR1; /* Compute idum=(IA1*idum) % IM1 without */
  if (*idum < 0) *idum += IM1;   /* overflows by Schrage's method */
  k=idum2/IQ2;
  idum2=IA2*(idum2-k*IQ2)-k*IR2; /* Compute idum2=(IA2*idum) % IM2 likewise */
  if (idum2 < 0) idum2 += IM2;
  j=(int)(iy/NDIV);              /* Will be in the range 0...NTAB-1 */
  iy=iv[j]-idum2;                /* Here idum is shuffled, idum and idum2 */
  iv[j] = *idum;                 /* are combined to generate output */
  if (iy < 1) iy += IMM1;
  if ((temp=AM*iy) > RNMX) return RNMX; /* No endpoint values */
  else return temp;
}

#undef IM1
#undef IM2
#undef AM
#undef IMM1
#undef IA1
#undef IA2
#undef IQ1
#undef IQ2
#undef IR1
#undef IR2
#undef NTAB
#undef NDIV
#undef RNMX

/* ========================================================================== */
