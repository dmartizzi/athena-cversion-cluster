#include "copyright.h"
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"
#include "prob/math_functions.h"

/* ========================================================================== */
/* Prototypes and Definitions */

/* Global Variables for use in Potential and Boundary Conditions */
/* -------------------------------------------------------------------------- */
static Real vrms, amp, Tigm,dxmin,dceil,dfloor;
static int gnx;
static Real mu=0.62, mue=1.18;

static Real mytanh(const Real x); 


/* Read in the density perturbations from a file... */
/* -------------------------------------------------------------------------- */
static void import_velocityperturb(DomainS *pDomain, char *pert_file);
static int n_entries, Ncells;
// static int *ix1,*ix2,*ix3;
static Real ***pertx;
static Real ***perty;
static Real ***pertz;
static char *pert_file;
static char *vpot_file;

#ifdef MHD
static Real ***az;
static Real ***ay;
static Real ***ax;
#endif 
static void import_vectorpotential(DomainS *pDomain, char *vpot_file);

/* -------------------------------------------------------------------------- */
static void set_vars(Real time);       /* use in problem() and read_restart() */
static void enforce_floor(DomainS *pDomain);
static void drive(DomainS *pDomain);

/* end prototypes and definitions */
/* ========================================================================== */


/* ========================================================================== */
/* Create the initial condition and start the simulation! */
void problem(DomainS *pDomain)
{
  GridS *pGrid = pDomain->Grid;
  int i=0,j=0,k=0;
  int is,ie,js,je,ks,ke,iprob,ku;
  Real x1,x2,x3, r, s;
  Real rho, P, v, ME, KE, T;
  int ierr;
  
  set_vars(pGrid->time);
  ath_pout(0,"pert_file = %s \n", pert_file);
  import_velocityperturb(pDomain, pert_file);
  import_vectorpotential(pDomain, vpot_file);
  ath_pout(0,"Read them in \n");

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;
  

/* =========================================================================================
  Set the static initial conditions - then add in the velocity perturbations
  ========================================================================================== */
  ath_pout(0,"Initial cond1 \n");
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        // ath_pout(0,"pertx[k,j,i]=%f perty[k,j,i]=%f pertz[k,j,i]=%f\n",pertx[k][j][i],perty[k,j,i],pertz[k,j,i]);
        pGrid->U[k][j][i].d = 1.0; //or whatever you end up setting it to in the unit system
        pGrid->U[k][j][i].M1 = pertx[k-4][j-4][i-4]*pGrid->U[k][j][i].d;
        pGrid->U[k][j][i].M2 = perty[k-4][j-4][i-4]*pGrid->U[k][j][i].d;
        pGrid->U[k][j][i].M3 = pertz[k-4][j-4][i-4]*pGrid->U[k][j][i].d;
        KE = (SQR(pGrid->U[k][j][i].M1) +
              SQR(pGrid->U[k][j][i].M2) +
              SQR(pGrid->U[k][j][i].M3)) / (2.0 * pGrid->U[k][j][i].d);
        P = Tigm*pGrid->U[k][j][i].d;
        pGrid->U[k][j][i].E = KE + P/Gamma_1;
#if (NSCALARS > 0)
#ifdef EXPLICIT_HEATING
	pGrid->U[k][j][i].s[0] = P/pow(pGrid->U[k][j][i].d,Gamma_1); // Conserved entropy field
	pGrid->U[k][j][i].s[1] = 0.0; // Heating field
	pGrid->U[k][j][i].s[2] = 0.0; // Driving field
#endif 
#endif
     }
    }
  }

  ath_pout(0,"Initial cond2 \n");
#ifdef MHD
/* boundary conditions on interface B */
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie+1; i++) {
        pGrid->B1i[k][j][i] = (az[k-4][j-4+1][i-4] - az[k-4][j-4][i-4])/pGrid->dx2 -
                                    (ay[k-4+1][j-4][i-4] - ay[k-4][j-4][i-4])/pGrid->dx3;
      }
    }
  }
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je+1; j++) {
      for (i=is; i<=ie; i++) {
        pGrid->B2i[k][j][i] = (ax[k+1-4][j-4][i-4] - ax[k-4][j-4][i-4])/pGrid->dx3 -
                                    (az[k-4][j-4][i+1-4] - az[k-4][j-4][i-4])/pGrid->dx1;
      }
    }
  }
  if (ke > ks) {
    ku = ke+1;
  } else {
    ku = ke;
  }
  for (k=ks; k<=ku; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        pGrid->B3i[k][j][i] = (ay[k-4][j-4][i+1-4] - ay[k-4][j-4][i-4])/pGrid->dx1 -
                                    (ax[k-4][j+1-4][i-4] - ax[k-4][j-4][i-4])/pGrid->dx2;
      }
    }
  }
#endif

/* initialize total energy and cell-centered B */

#if defined MHD || !defined ISOTHERMAL
#ifdef MHD
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        pGrid->U[k][j][i].B1c = 0.5*(pGrid->B1i[k][j][i  ] +
                                     pGrid->B1i[k][j][i+1]);
        pGrid->U[k][j][i].B2c = 0.5*(pGrid->B2i[k][j  ][i] +
                                     pGrid->B2i[k][j+1][i]);
        if (ke > ks)
          pGrid->U[k][j][i].B3c = 0.5*(pGrid->B3i[k  ][j][i] +
                                       pGrid->B3i[k+1][j][i]);
        else
          pGrid->U[k][j][i].B3c = pGrid->B3i[k][j][i];
        pGrid->U[k][j][i].E += 0.5*(SQR(pGrid->U[k][j][i].B1c)
                                  + SQR(pGrid->U[k][j][i].B2c)
                                  + SQR(pGrid->U[k][j][i].B3c));
      }
    }
  }
#endif
#endif

#ifdef MHD
  free_3d_array((void***)az);
  free_3d_array((void***)ay);
  free_3d_array((void***)ax);
#endif

  enforce_floor(pDomain);
  return;
}
/*=================================================================================*/
/*==========================         end problem         ==========================*/
/*=================================================================================*/


/*=================================================================================*/
/*=============================    begin set_vars    ==============================*/
/*=================================================================================*/
/* use in problem() and read_restart()*/
static void set_vars(Real time)
{
  Real dx;

#ifdef STATIC_MESH_REFINEMENT
  int ir, irefine, nlevels;

#ifdef MPI_PARALLEL
  Real my_dxmin;
  int ierr;
#endif  /* MPI_PARALLEL */
#endif  /* STATIC_MESH_REFINEMENT */

  int iseed;

  iseed = -10;
#ifdef MPI_PARALLEL
  iseed -= myID_Comm_world;
#endif  /* MPI_PARALLEL */
  srand(iseed);


  pert_file = par_gets("problem", "pert_file");
  vpot_file = par_gets("problem", "vpot_file");
  amp       = par_getd_def("problem", "amp", 1e-4); //amplitude of magnetic field
  vrms      = par_getd_def("problem", "vrms", 1e-4); //RMS velocity
  Tigm      = par_getd_def("problem", "Tigm", 1e-1); //IGM temperature
  dceil     = par_getd_def("problem", "dceil", 1e5); 
  dfloor    = par_getd_def("problem", "dfloor", 1e-5); 

  /* calculate dxmin.  this is pretty bad code... */
  dx = par_getd("domain1", "x1max")-par_getd("domain1", "x1min");
  dx /= (Real) par_geti("domain1", "Nx1");
  dxmin = dx;

  if (par_geti("domain1", "Nx2")>1) {
    dx = (par_getd("domain1", "x2max")-par_getd("domain1", "x2min"));
    dx /= (Real) par_geti("domain1", "Nx2");
    dxmin = MIN(dxmin, dx);
  }

  if (par_geti("domain1", "Nx3")>1) {
    dx = (par_getd("domain1", "x3max")-par_getd("domain1", "x3min"));
    dx /= (Real) par_geti("domain1", "Nx3");
    dxmin = MIN(dxmin, dx);
  }

#ifdef STATIC_MESH_REFINEMENT
  irefine = 1;
  nlevels = par_geti("job", "num_domains");
  for (ir=1; ir<nlevels; ir++) irefine *= 2;
  dxmin /= (Real) irefine;

  /* sync over all processors */
#ifdef MPI_PARALLEL
  my_dxmin = dxmin;
  ierr = MPI_Allreduce(&my_dxmin, &dxmin, 1,
                       MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
#endif /* MPI_PARALLEL */
#endif  /* STATIC_MESH_REFINEMENT */

  return;
}
/* end problem() */
/* ========================================================================== */







/* ========================================================================== */
static void enforce_floor(DomainS *pDomain)
{
  /* Enforce T>=Tigm and d>dfloor everywhere */
  GridS *pGrid = pDomain->Grid;
  PrimS W;
  ConsS U;
  Real T,d,P;
  Real x1,x2,x3, r;
  int i,j,k,ierr;
  int is,ie,js,je,ks,ke;
  Real mass; // this is a holder for the records

  Real my_m_added = 0.;
  Real my_m_removed = 0.;
  Real m_added = 0.;
  Real m_removed = 0.;

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;


  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        r = sqrt(x1*x1+x2*x2+x3*x3);
        W = Cons_to_Prim(&(pGrid->U[k][j][i]));
        d = W.d;
        if (d > dceil){
          // ath_pout(-1, "[enforce_floor]: hit density ceiling at %f, density was %f\n", r,d);
          mass = (d - dceil)* pGrid->dx1 * pGrid->dx2 * pGrid->dx3;
          my_m_removed += mass;
          W.d = dceil;
          U = Prim_to_Cons(&W);
          pGrid->U[k][j][i].E  = U.E;
          pGrid->U[k][j][i].d  = U.d;
          pGrid->U[k][j][i].M1 = U.M1;
          pGrid->U[k][j][i].M2 = U.M2;
          pGrid->U[k][j][i].M3 = U.M3;
        }
        if (d < dfloor){
          mass = (dfloor - d)* pGrid->dx1 * pGrid->dx2 * pGrid->dx3;
          my_m_added += mass;
          W.d = dfloor;
          U = Prim_to_Cons(&W);
          pGrid->U[k][j][i].E  = U.E;
          pGrid->U[k][j][i].d  = U.d;
          pGrid->U[k][j][i].M1 = U.M1;
          pGrid->U[k][j][i].M2 = U.M2;
          pGrid->U[k][j][i].M3 = U.M3;
        }
        P = W.P;
        T = P/d;
        if (T < 0.1*Tigm){
          T = 0.1*Tigm;
          W.P = W.d*Tigm;
          U = Prim_to_Cons(&W);
          pGrid->U[k][j][i].E  = U.E;
        }
      }
    }
  }

/// I have to be able to do this for the array all at once
#ifdef MPI_PARALLEL 
  ierr = MPI_Allreduce(&my_m_added, &m_added, 1,
                       MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_m_removed, &m_removed, 1,
                       MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
  m_added = my_m_added;
  m_removed = my_m_removed;
#endif /* MPI_PARALLEL */

  ath_pout(0, "[Mass Added]: t = %f \t %e \n",pGrid->time, m_added);
  ath_pout(0, "[Mass Remov]: t = %f \t %e \n",pGrid->time, m_removed);
}
/* ========================================================================== */



/*==============================================================================
 * PROBLEM USER FUNCTIONS:
 * problem_write_restart() - writes problem-specific user data to restart files
 * problem_read_restart()  - reads problem-specific user data from restart files
 * get_usr_expr()          - sets pointer to expression for special output data
 * get_usr_out_fun()       - returns a user defined output function pointer
 * get_usr_par_prop()      - returns a user defined particle selection function
 * Userwork_in_loop        - problem specific work IN     main loop
 * Userwork_after_loop     - problem specific work AFTER  main loop
 *----------------------------------------------------------------------------*/


void problem_write_restart(MeshS *pM, FILE *fp)
{
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp)
{
  set_vars(pM->time);
  return;
}

ConsFun_t get_usr_expr(const char *expr)
{
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name){
  return NULL;
}

/* "inner" bc: set density, temperature, and velocity at r = 2 rvir. */
/*   use global variables rho_out, Tigm, and rvir */
static void drive(DomainS *pDomain)
{
/* -----------------------------------------------------------------------------------------------------------------
    
   drive the turbulence
   ----------------------------------------------------------------------------------------------------------------- */

  GridS *pGrid = pDomain->Grid;
  int i=0,j=0,k=0;
  int is,ie,js,je,ks,ke;
  Real x1,x2,x3, r,v,s;
#ifndef ISOTHERMAL
  Real KE, T, rho, ME, KE_old;
#endif  /* NOT ISOTHERMAL */

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;
  for (k=ks; k<=ke; k++) {
    for (j=js; j<=je; j++) {
      for (i=is; i<=ie; i++) {
        cc_pos(pGrid,i,j,k,&x1,&x2,&x3);
        rho = pGrid->U[k][j][i].d;
        KE = (SQR(pGrid->U[k][j][i].M1) +
              SQR(pGrid->U[k][j][i].M2) +
              SQR(pGrid->U[k][j][i].M3)) / (2.0 * rho);
        pGrid->U[k][j][i].E -= KE; // take off the old KE
	KE_old = KE;

        pGrid->U[k][j][i].M1 += pertx[k-4][j-4][i-4]*rho; // add the velocity
        pGrid->U[k][j][i].M2 += perty[k-4][j-4][i-4]*rho;
        pGrid->U[k][j][i].M3 += pertz[k-4][j-4][i-4]*rho;

        KE = (SQR(pGrid->U[k][j][i].M1) +
              SQR(pGrid->U[k][j][i].M2) +
              SQR(pGrid->U[k][j][i].M3)) / (2.0 * rho); // find the new KE
        pGrid->U[k][j][i].E += KE; // add in the new KE // Davide: why minus? I changed it to a plus.

	pGrid->U[k][j][i].s[2] = KE-KE_old;
      }
    }
  }
  return;
}





/* calculate rho and T at the turnaround radius and call inner_bc() */
void Userwork_in_loop(MeshS *pM)
{
  GridS *pG;
  int i,j,k,nl,nd,ierr;
  int is,ie,il,iu,js,je,jl,ju,ks,ke,kl,ku;
  Real dx1, dx2, dx3;
  Real my_drive, my_heat, total_drive, total_heat;
  
  my_drive = 0.0;
  my_heat = 0.0;
  for (nl=0; nl<=(pM->NLevels)-1; nl++) {
    for (nd=0; nd<=(pM->DomainsPerLevel[nl])-1; nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) {
#if (NSCALARS > 0)
#ifdef EXPLICIT_HEATING
        is = pM->Domain[nl][nd].Grid->is; ie = pM->Domain[nl][nd].Grid->ie;
        js = pM->Domain[nl][nd].Grid->js; je = pM->Domain[nl][nd].Grid->je;
        ks = pM->Domain[nl][nd].Grid->ks; ke = pM->Domain[nl][nd].Grid->ke;
	for (k=ks; k<=ke; k++) {
          for (j=js; j<=je; j++) {
            for (i=is; i<=ie; i++) {
	      dx1 = pM->Domain[nl][nd].dx[0];
	      dx2 = pM->Domain[nl][nd].dx[1];
	      dx3 = pM->Domain[nl][nd].dx[2];
	      pM->Domain[nl][nd].Grid->U[k][j][i].s[1] = pM->Domain[nl][nd].Grid->U[k][j][i].s[1]/(pM->dt);
	      my_heat += pM->Domain[nl][nd].Grid->U[k][j][i].s[1]*dx1*dx2*dx3;
	      pM->Domain[nl][nd].Grid->U[k][j][i].s[2] = pM->Domain[nl][nd].Grid->U[k][j][i].s[2]/(pM->dt);
	      my_drive += pM->Domain[nl][nd].Grid->U[k][j][i].s[2]*dx1*dx2*dx3;
	    }
	  }
	}
#endif
#endif
        drive(&(pM->Domain[nl][nd]));
        enforce_floor(&(pM->Domain[nl][nd]));
      }
    }
  }

#ifdef MPI_PARALLEL
  ierr = MPI_Allreduce(&my_heat, &total_heat, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  ierr = MPI_Allreduce(&my_drive, &total_drive, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
  total_heat = my_heat;
  total_drive = my_drive;
#endif

  ath_pout(0,"[problem]: heating_rate = %e, driving_rate = %e \n", total_heat, total_drive);

  if(pM->dt < 1e-12)
    ath_error("[Userwork_in_loop]: dt has gotten too small %e\n", pM->dt);
  return;
}

void Userwork_after_loop(MeshS *pM)
{
  return;
}
/* end userwork */
/* ========================================================================== */


/* ========================================================================== */
/* gravity */
static Real mytanh(const Real x)
{
  return 0.5 * (1.0 + tanh(x));
}




/* ========================================================================== */
static void import_vectorpotential(DomainS *pDomain, char *vpot_file)
{
#ifdef MHD
  FILE *input;
  GridS *pGrid = pDomain->Grid;
  char buff[512];
  int ipx1,ipx2,ipx3;
  Real AZ, AY, AX;
  int nx1,nx2,nx3;
  char c1, c2;
  int i,j,k, ipert;
  int is,ie,js,je,ks,ke,Ngrids, Ncells, n_entries;
  Real x1, x2, x3;

  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;
  
  Ngrids = (pDomain->NGrid[0])*(pDomain->NGrid[1])*(pDomain->NGrid[2]);
  
  ath_pout(0, "[import_vectorpotential]: there are %d grids\n", Ngrids);
  
  if((input = fopen(vpot_file, "r")) == NULL)
    ath_error("[import_vectorpotential]: Could not read file: %s\n", vpot_file);

  /* Read lines from the file until we encounter n = %d */
  n_entries = 0;
  while (fgets(buff, sizeof(buff), input) != NULL) {
    if ((sscanf(buff, "%c %c %d", &c1, &c2, &n_entries) == 3)
        && (c1 == 'n') && (c2 == '=')){
      ath_pout(0, "[import_vectorpotential]: Reading %d lines from input file.\n",
               n_entries);
      break;
    }
  }
  rewind(input);

  nx1 = (ie-is)+3 ;
  nx2 = (je-js)+3 ;
  nx3 = (ke-ks)+3 ;

  if ((ax = (Real***)calloc_3d_array(nx3, nx2, nx1, sizeof(Real))) == NULL) {
    ath_error("[import_vectorpotential]: Error allocating memory for vector pot\n");
  }
  if ((ay = (Real***)calloc_3d_array(nx3, nx2, nx1, sizeof(Real))) == NULL) {
    ath_error("[import_vectorpotential]: Error allocating memory for vector pot\n");
  }
  if ((az = (Real***)calloc_3d_array(nx3, nx2, nx1, sizeof(Real))) == NULL) {
    ath_error("[import_vectorpotential]: Error allocating memory for vector pot\n");
  }
  
  ath_pout(0, "[import_vectorpotential]: starting dpert read in\n");
  while(fgets(buff, sizeof(buff), input) != NULL){
    if (sscanf(buff, "%i %i %i %le %le %le", &ipx1,&ipx2,&ipx3,&AX,&AY,&AZ) == 6){
      if ((is+pGrid->Disp[0]-4 <= ipx1) && (ie+pGrid->Disp[0]-4+2 >= ipx1) && (js+pGrid->Disp[1]-4 <= ipx2) && (je+pGrid->Disp[1]-4+2 >= ipx2) && (ks+pGrid->Disp[2]-4 <= ipx3) && (ke+pGrid->Disp[2]-4+2 >= ipx3)){
        ax[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)] = amp*AX;
        ay[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)] = amp*AY;
        az[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)] = amp*AZ;
      }
    }
  }
  ath_pout(0, "[import_vectorpotential]: Finished reading in the perturbation cube\n");
  fclose(input);
#endif
}



/* ========================================================================== */
static void import_velocityperturb(DomainS *pDomain, char *pert_file)
{
  FILE *input;
  GridS *pGrid = pDomain->Grid;
  char buff[512];
  int ipx1,ipx2,ipx3;
  Real rho;
  char c1, c2;
  int i,j,k, ipert, Nrefine;
  int nx1,nx2,nx3;
  Real px,py,pz;
  int is,ie,js,je,ks,ke,Ngrids, Ncells, n_entries;
  is = pGrid->is; ie = pGrid->ie;
  js = pGrid->js; je = pGrid->je;
  ks = pGrid->ks; ke = pGrid->ke;
  Ngrids = (pDomain->NGrid[0])*(pDomain->NGrid[1])*(pDomain->NGrid[2]);
  
  ath_pout(0, "[import_velocityperturb]: there are %d grids\n", Ngrids);
  
  if((input = fopen(pert_file, "r")) == NULL)
    ath_error("[import_velocityperturb]: Could not read file: %s\n", pert_file);

  /* Read lines from the file until we encounter n = %d */
  n_entries = 0;
  while (fgets(buff, sizeof(buff), input) != NULL) {
    if ((sscanf(buff, "%c %c %d", &c1, &c2, &n_entries) == 3)
        && (c1 == 'n') && (c2 == '=')){
      ath_pout(0, "[import_velocityperturb]: Reading %d lines from input file.\n",
               n_entries);
      break;
    }
  }
  // rewind(input);
  
  nx1 = (ie-is)+1 ;
  nx2 = (je-js)+1 ;
  nx3 = (ke-ks)+1 ;

  
  if ((pertx = (Real***)calloc_3d_array(nx3, nx2, nx1, sizeof(Real))) == NULL) {
    ath_error("[import_velocityperturb]: Error allocating memory for vector pot\n");
  }
  if ((perty = (Real***)calloc_3d_array(nx3, nx2, nx1, sizeof(Real))) == NULL) {
    ath_error("[import_velocityperturb]: Error allocating memory for vector pot\n");
  }
  if ((pertz = (Real***)calloc_3d_array(nx3, nx2, nx1, sizeof(Real))) == NULL) {
    ath_error("[import_velocityperturb]: Error allocating memory for vector pot\n");
  }

  ath_pout(0, "[import_velocityperturb]: starting dpert read in, nx1 = %i,nx2 = %i,nx3 = %i \n", nx1, nx2,nx3);
  while(fgets(buff, sizeof(buff), input) != NULL){
    if (sscanf(buff, "%i %i %i %le %le %le", &ipx1,&ipx2,&ipx3,&px,&py,&pz ) == 6){
      if ((is+pGrid->Disp[0]-4 <= ipx1) && (ie+pGrid->Disp[0]-4 >= ipx1) && (js+pGrid->Disp[1]-4 <= ipx2) && (je+pGrid->Disp[1]-4 >= ipx2) && (ks+pGrid->Disp[2]-4 <= ipx3) && (ke+pGrid->Disp[2]-4 >= ipx3)){
        pertx[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)] = vrms*px;
        perty[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)] = vrms*py;
        pertz[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)] = vrms*pz;
        // ath_pout(0,"[import_velocityperturb] i,j,k, pertx[i][j][k], perty[i][j][k], pertz[i][j][k] = %i %i %i %le %le %le\n", ipx1-(is+pGrid->Disp[0]-4),ipx2-(js+pGrid->Disp[1]-4),ipx3-(ks+pGrid->Disp[2]-4),pertx[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)],perty[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)],pertz[ipx3-(ks+pGrid->Disp[2]-4)][ipx2-(js+pGrid->Disp[1]-4)][ipx1-(is+pGrid->Disp[0]-4)]);
      }
    }
  }
  ath_pout(0, "[import_velocityperturb]: check pertz[3][3][3]=%f\n",pertz[3][3][3]);
  ath_pout(0, "[import_velocityperturb]: Finished reading in the perturbation cube\n");

  fclose(input);
}
/* end import_velocityperturb() */
/* ========================================================================== */

