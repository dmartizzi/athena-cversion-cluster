README written by Davide Martizzi, April 5, 2018

Athena-Cversion is a code developed by J. Stone, T. Gardiner, and P. Teuben, see Stone et al., ApJS, 178, 137 (2008), Stone & Gardiner, NewA, 14, 139 (2009). Please, send detailed questions regarding the code to these developers. 

See documentation here: https://princetonuniversity.github.io/Athena-Cversion/

This version has been modified by Davide Martizzi to perform simulations of jet heating in cluster cores and it includes a new diagnostic of the heating and cooling rate of the gas. The results were published by Martizzi et al. (2019): https://ui.adsabs.harvard.edu/#abs/2019MNRAS.483.2465M/abstract. Please, cite this paper if you decide to use this version of the code. 

Please configure using one of the following options.

./configure --with-problem=cluster_entropy_powlaw_heating --with-gas=hydro --with-flux=hllc --with-order=2p --with-integrator=ctu --enable-mpi -enable-explicit-heating -with-nscalars=5 -enable-smr -enable-conduction

./configure --with-problem=cluster_entropy_powlaw_heating --with-gas=hydro --with-flux=hlle --with-order=2p --with-integrator=vl --enable-fofc --enable-mpi -enable-explicit-heating -with-nscalars=5 -enable-smr -enable-conduction